package com.gree.shyun.autopage.task;

import com.gree.shyun.autopage.Const;
import com.gree.shyun.autopage.bean.*;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.db.sqlite.Selector;
import com.gree.shyun.server.db.sqlite.WhereBuilder;
import com.gree.shyun.server.taskpool.async.bean.Param;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.ITimeoutWorker;
import com.gree.shyun.server.taskpool.async.callback.IWorker;
import com.gree.shyun.server.taskpool.async.wrapper.WorkerWrapper;
import com.gree.shyun.service.ProcessService;
import com.gree.shyun.util.json.JsonMananger;

import java.util.*;


public class AutoPageTask implements IWorker<Param, Result> {

    @Override
    public Result action(Param param, Map<String, WorkerWrapper> allWrappers) {
        String method = param.get();
        Object data = param.get("data");
        RequestParam p = JsonMananger.jsonToBean(data.toString(),RequestParam.class);
        switch (p.getAction()){
            case "deletePage":{
                String id = p.getData().get("id");
                DbHelper.deleteById(Page.class,id);
                List<Tab> tabs = DbHelper.findAll(Selector.from(Tab.class).where("pageId","=",id));
                if(tabs!=null) {
                    for (Tab tab : tabs) {
                        DbHelper.delete(tab);
                        DbHelper.delete(Item.class, WhereBuilder.b("tabId", "=", tab.getId()));
                    }
                }
            }
            return ProcessService.success(method,"已删除");
            case "deleteTab":{
                String id = p.getData().get("id");
                DbHelper.deleteById(Tab.class,id);
                DbHelper.delete(Item.class,WhereBuilder.b("tabId","=",id));
            }
            return ProcessService.success(method,"已删除");
            case "deleteItem":{
                String id = p.getData().get("id");
                DbHelper.deleteById(Item.class,id);
                return ProcessService.success(method,id);
            }
            case "saveOrUpdatePage": {
                String name = p.getData().get("name");
                String id = p.getData().get("id");
                Page page = new Page();
                if ("".equals(id)) {
                    page.setId(UUID.randomUUID().toString());
                    page.setCreateDate(new Date());
                } else {
                    page.setId(id);
                }
                page.setName(name);
                DbHelper.saveOrUpdate(page,"name");
                return ProcessService.success(method, page);
            }
            case "saveOrUpdateTab":{
                String name = p.getData().get("name");
                String id  = p.getData().get("id");
                String pageId = p.getData().get("pageId");
                String orderBy = p.getData().get("orderBy");
                Tab tab = new Tab();
                if ("".equals(id)) {
                    tab.setId(UUID.randomUUID().toString());
                    tab.setCreateDate(new Date());
                } else {
                    tab.setId(id);
                }
                tab.setPageId(pageId);
                tab.setName(name);
                tab.setOrderBy(Integer.parseInt(orderBy));
                DbHelper.saveOrUpdate(tab,"pageId","name","orderBy");
                return ProcessService.success(method, tab);
            }
            case "saveOrUpdateItem":{
                 String tabId= p.getData().get("tabId");
                 String sn= p.getData().get("sn");//编号
                 String title= p.getData().get("title");//标题
                String hint= p.getData().get("hint");//默认提示
                String tips= p.getData().get("tips");//错误提示
                String type= p.getData().get("type");//类型
                String value= p.getData().get("value");//值
                String enabled= p.getData().get("enabled");//是否禁用
                String required= p.getData().get("required");//必填
                String danw= p.getData().get("danw");//单位
                String length= p.getData().get("length");//最大长度
                String hide= p.getData().get("hide");//隐藏
                String color= p.getData().get("color");//文本颜色
                String regexVerify= p.getData().get("regexVerify");//正则验证
                String regexTips= p.getData().get("regexTips");//正则验证错误提示
                String full= p.getData().get("full");//是否占整行
                String orderBy= p.getData().get("orderBy");//排序
                String id  = p.getData().get("id");

                Item item = new Item();
                if ("".equals(id)) {
                    item.setId(UUID.randomUUID().toString());
                } else {
                    item.setId(id);
                }
                item.setTabId(tabId);
                item.setSn(sn);
                item.setTitle(title);
                item.setHint(hint);
                item.setTips(tips);
                item.setType(Integer.parseInt(type));
                item.setValue(value);
                item.setEnabled(Boolean.valueOf(enabled));
                item.setRequired(Boolean.valueOf(required));
                item.setDanw(danw);
                item.setLength(Integer.valueOf(length));
                item.setOrderBy(Integer.valueOf(orderBy));
                item.setHide(Boolean.valueOf(hide));
                item.setColor(Integer.parseInt(color));
                item.setRegexVerify(regexVerify);
                item.setRegexTips(regexTips);
                item.setFull(Boolean.valueOf(full));
                item.setOrderBy(Integer.parseInt(orderBy));
                item.setCreateDate(new Date());
                DbHelper.saveOrUpdate(item);
                return ProcessService.success(method, item);
            }
            case "getAllPage": {
                List<Page> pages = DbHelper.findAll(Selector.from(Page.class).orderBy("createDate"));
                return ProcessService.success(method, pages);
            }
            case "getPage": {
                String id = p.getData().get("id");

                String sql = "update "+DbHelper.getTableName(Page.class)+" set active = 0 where active = 1";
                String sql2 ="update "+DbHelper.getTableName(Page.class)+" set active = 1  where id = '"+id+"'";
                DbHelper.exec_nonquery(sql);
                DbHelper.exec_nonquery(sql2);

                Page page = DbHelper.findById(Page.class, id);
                List<Tab> tabs = DbHelper.findAll(Selector.from(Tab.class).where("pageId", "=", id).orderBy("orderBy"));
               if(tabs!=null){
                   for (Tab tab : tabs) {
                       List<Item> items = DbHelper.findAll(Selector.from(Item.class).where("tabId", "=", tab.getId()).orderBy("orderBy"));
                       tab.setItems(items);
                   }
               }
                page.setTabs(tabs);
                return ProcessService.success(method, page);
            }
            case "getComponent":
                return ProcessService.success(method,Const.component);
            case "getTab": {
                String id = p.getData().get("id");
                String pageId = p.getData().get("pageId");
                String sql = "update "+DbHelper.getTableName(Tab.class)+" set active = 0 where active = 1 and  pageId = '"+pageId+"';";
                String sql2 ="update "+DbHelper.getTableName(Tab.class)+" set active = 1  where id = '"+id+"'  and  pageId = '"+pageId+"';";
                DbHelper.exec_nonquery(sql);
                DbHelper.exec_nonquery(sql2);

                List<Item> items = DbHelper.findAll(Selector.from(Item.class).where("tabId", "=", id).orderBy("orderBy"));
                if(items==null){
                    items = new ArrayList<>();
                }
                return ProcessService.success(method, items);
            }
            case "getNewItem":{
                String stype = p.getData().get("type");
                String orderBy = p.getData().get("orderBy");
                String tabId = p.getData().get("tabId");
                Item item = new Item();
                item.setId(UUID.randomUUID().toString());
                item.setType(Integer.parseInt(stype));
                item.setName(Const.getCompNameByType(item.getType()));
                item.setOrderBy(Integer.parseInt(orderBy));
                item.setTabId(tabId);
                DbHelper.save(item);
                return ProcessService.success(method,item);
            }
            case "updateItemOrderBy":{
                String id = p.getData().get("id");
                String orderBy = p.getData().get("orderBy");
                DbHelper.exec_nonquery("update "+DbHelper.getTableName(Item.class)+" set orderBy = "+orderBy +" where id = '"+id+"'");
                return ProcessService.success(method,orderBy);
            }
        }
        return ProcessService.error(method,500,"无此方法");
    }
}
