package com.gree.shyun.autopage.bean;

import com.gree.shyun.entity.Base;
import com.gree.shyun.server.db.annotation.Table;

@Table
public class Item extends Base {
    private String tabId;

    private String sn;//编号
    private String title;//标题
    private String hint;//默认提示
    private String tips;//错误提示
    private int type;//类型
    private String value;//值
    private boolean enabled;//是否禁用
    private boolean required;//必填
    private String danw;//单位
    private int length;//最大长度
    private boolean hide;//隐藏
    private int color;//文本颜色
    private String btnText;//按钮文本
    private String regexVerify;//正则验证
    private String regexTips;//正则验证错误提示
    private boolean full;//是否占整行

    private int orderBy;//排序

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDanw() {
        return danw;
    }

    public void setDanw(String danw) {
        this.danw = danw;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }

    public String getRegexVerify() {
        return regexVerify;
    }

    public void setRegexVerify(String regexVerify) {
        this.regexVerify = regexVerify;
    }

    public String getRegexTips() {
        return regexTips;
    }

    public void setRegexTips(String regexTips) {
        this.regexTips = regexTips;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }
}
