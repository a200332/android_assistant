package com.gree.shyun.autopage.bean;

import java.util.HashMap;
import java.util.Map;

public class RequestParam {
    private String action;
    private Map<String,String> data = new HashMap<>();

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
