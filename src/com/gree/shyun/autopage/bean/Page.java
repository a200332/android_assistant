package com.gree.shyun.autopage.bean;

import com.gree.shyun.entity.Base;
import com.gree.shyun.server.db.annotation.Table;

import java.util.ArrayList;
import java.util.List;

@Table
public class Page extends Base {
    private String name;
    private List<Tab> tabs = new ArrayList<>();

    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

    public void setTabs(List<Tab> tabs) {
        this.tabs = tabs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
