package com.gree.shyun.autopage;

public class Const {
    public static final Object[][] component = {
            {"默认输入框",0},
            {"默认选择框",1},
            {"日期选择框",2},
            {"多行文本输入框",3},
            {"单选框",4},
            {"标题栏",5},
            {"图片列表",6},
            {"视频列表",7},
            {"分割线",8},
            {"选择框(带按钮)",9},
            {"开关",10},
            {"按钮组",11},
            {"选择框(带按钮,可编辑文本)",12},
            {"数字输入框",13},
            {"图文框",14},
            {"多条件筛选框",15},
            {"选择框(带按钮,删除)",16},
            {"多选框",17},
            {"密码输入框",18},
            {"单选框(横向)",19},
            {"下拉列表",20}
    };
    public static String getCompNameByType(int type){
        for(Object[] o:component){
            if((int)o[1]==type){
                return (String)o[0];
            }
        }
        return "";
    }
}
