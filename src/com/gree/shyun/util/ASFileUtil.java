package com.gree.shyun.util;

import com.gree.shyun.bean.FileInfo;
import com.gree.shyun.bean.View;
import com.gree.shyun.bean.XmlFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ASFileUtil {
    /**
     * 获取android studio当前打开的文件
     * @return
     */
    public static FileInfo getFileInfo() {
//        List<String> task = exec("tasklist /FI \"Imagename eq studio64.exe\"");
//        String pid = getAsPid(task);
//        if(pid==null){
//            return null;
//        }
//        List<String> ex = exec("tasklist /V /fo csv /FI \"PID eq "+pid+"\" ");
//        String path = getFilePath(ex);
//        if(path==null){
//            return null;
//        }
//
//        FileInfo fileInfo = new FileInfo();
//
//        String left = path.substring(0,path.indexOf("]")).trim();
//        String right = path.substring(path.indexOf("- ...")+5).trim();
//        right = right.substring(0,right.indexOf("-")).trim();
//        if(right.contains("[")){
//            right = right.substring(0,right.indexOf("[")-1);
//        }
//        if(right!=null&&right.contains("\\java\\")){
//            String modelPath = right.substring(0,right.indexOf("\\java\\"));
//            modelPath = left+modelPath;
//            fileInfo.setModelPath(modelPath);
//        }

        FileInfo fileInfo = new FileInfo();
        fileInfo.setRootPath("F:\\greeshyun\\app");
        fileInfo.setFilePath("\\src\\main\\java\\com\\gree\\shyun\\ui\\LoginActivity.java");
        fileInfo.setAllPath("F:\\greeshyun\\app\\src\\main\\java\\com\\gree\\shyun\\ui\\LoginActivity.java");
        fileInfo.setModelPath("F:\\greeshyun\\app\\src\\main\\");

        List<XmlFile> layouts = getLayoutPath(fileInfo.getAllPath());
        if(layouts!=null){
            fileInfo.setLayouts(layouts);
        }
        return fileInfo;
    }
    private static String getFilePath(List<String> task){
        if(task==null||task.size()==0){
            return null;
        }
        String title = task.get(task.size()-1);
        title=title.substring(title.indexOf("[")+1);
        return title;
    }
    private static String getAsPid(List<String> task){
        if(task==null||task.size()==0){
            return null;
        }
        String line =null;
        for(String s:task){
            if(s.contains("studio64.exe")){
                line = s;
                break;
            }
        }
        if(line==null){
            return null;
        }
        String pid = line.substring(line.indexOf("exe")+3,line.indexOf("Console"));
        return pid.trim();
    }

    /**
     * 获取文件格式
     * @param path
     * @return
     */
    public static String getFileFormat(String path){
        if(path==null||!path.contains(".")){
            return null;
        }
        return path.substring(path.lastIndexOf(".")+1);
    }
    /**
     * 获取activity 或 adapter里的布局文件
     * @param path
     * @return
     */
    public static List<XmlFile> getLayoutPath(String path){
        if(path==null){
            return null;
        }
        String splix = getFileFormat(path);
        if(!"java".equals(splix)){
            return null;
        }
       String  name = path.substring(path.lastIndexOf("/")+1,path.indexOf("."));
        name = name.toLowerCase();
        if(name.contains("activity")||name.contains("adapter")||name.contains("dialog")||name.contains("fragement")){
            String regex = "R\\.layout\\.(.*?)\\)";
            if(name.contains("fragement")){
                regex = "R\\.layout\\.(.*?)\\;";
            }
            List<XmlFile> list = new ArrayList<>();
            Pattern pattern = Pattern.compile(regex);
            try {
                String txt = JsonXmlUtils.fileToString(path);
                Matcher m = pattern.matcher(txt);
                String left = path.substring(0,path.indexOf("main"));
                while (m.find()) {
                    int i = 1;
                    String str = m.group(i);
                    if(str.contains(",")){
                        str = str.substring(0,str.indexOf(","));
                    }
                    if(str.contains(")")){
                        str = str.replace(")","");
                    }
                    String p=left+"main\\res\\layout\\"+str+".xml";
                    XmlFile xmlFile = new XmlFile();
                    xmlFile.setName(str);
                    xmlFile.setPath(p);
                    list.add(xmlFile);
                    i++;
                }
                return list;
            } catch (IOException e) {
                System.out.println("出错:"+e.getMessage());
            }
        }
        return null;
    }


    /**
     *  执行cmd命令
     * @param cmd
     * @return
     */
    public static   List<String> exec(String cmd){
        List<String> list=new ArrayList();
        try{
            Process process = Runtime.getRuntime().exec(cmd);
            Scanner in=new Scanner(process.getInputStream());
            while(in.hasNextLine()){
                String p = in.nextLine();
                list.add(p);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }

    public static View getView(String path){
        if(path==null){
            return null;
        }
        String sp = getFileFormat(path);
        if(!"xml".equals(sp)){
            return null;
        }

        return null;
    }

    /**
     *  获取长日志
     * @param json
     * @return
     */
    public static String getLog(String json){
            String[] res = json.split("\n");
            String ret = "";
            String split="-------------------";
            for(int i =  0 ;i<res.length;i++){
                String r = res[i];
                String rr = r.substring(r.indexOf(split)+split.length());
                if(rr.length()>5&&rr.startsWith("body=")){
                    rr = rr.substring(5);
                }
                ret+= rr;
            }
            return ret;
    }
}
