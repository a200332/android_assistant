package com.gree.shyun.util;

import com.gree.shyun.server.db.util.LogUtils;
import com.gree.shyun.server.db.util.TextUtils;
import com.gree.shyun.entity.Form;
import com.gree.shyun.entity.FormData;
import com.gree.shyun.entity.FormPage;
import com.gree.shyun.util.json.JsonMananger;

import java.util.ArrayList;
import java.util.List;

public class FormUtil {
    public static String formPageToCode(List<FormPage> formPages){
        if(formPages.size()==0){
            return "";
        }
        for(int i = 0 ;i<formPages.size();i++){
            formPages.get(i).setPage_id(i+"");
        }
        String code = "package com.gree.shyun.ui.orderdetail.module;\n\n";
        code+="import com.gree.shyun.ui.orderdetail.annotation.GroupColumn;\n" +
                "import com.gree.shyun.ui.orderdetail.com.gree.shyun.util.FormUtil;\n" +
                "\n" +
                "import java.util.ArrayList;\n" +
                "import java.util.List;\n" +
                "import java.util.UUID;\n";
        code+="public class "+formPages.get(0).getPage_class()+" {\n";

        String init = "     public "+formPages.get(0).getPage_class()+"(Order order){\n";
        List<String> gets = new ArrayList<>();
        for(FormPage formPage:formPages){
            String lowerColumn = CommonUtil.firstToLowerCase(formPage.getPage_column());
            code+="     @GroupColumn(pid="+formPage.getPage_id()+",title=\""+formPage.getPage_title()+"\")\n";
            code+="     public "+formPage.getPage_column()+" "+lowerColumn+";\n";
            init +="        this."+lowerColumn+"= get"+formPage.getPage_column()+"(order.getId());\n";
            String g = "    public "+formPage.getPage_column()+" get"+formPage.getPage_column()+"(String pid){\n";
            g+="        "+lowerColumn+" =  FormUtil.findByPid("+formPage.getPage_column()+".class,pid);\n";
            g+="        if("+lowerColumn+"==null){\n";
            g+="            "+lowerColumn+" = new "+formPage.getPage_column()+"();\n";
            g+="            "+lowerColumn+".setId(UUID.randomUUID().toString());\n";
            g+="            "+lowerColumn+".setPid(order.getId());\n";
            g+="        }\n";
            g+="        return "+lowerColumn+";\n";
            g+="   }\n";
            gets.add(g);
        }
        code +=init;
        code +="    }\n";
        for(String s:gets){
            code+=s;
        }
        code+="}";
        return code;
    }
    public static String formToCode(FormData formData){
        List<Form> forms = formData.getForms();
        if(forms.size()==0){
            return "";
        }
        for(int i = 0 ;i<forms.size();i++){
            Form form = forms.get(i);
            if(TextUtils.isEmpty(form.getId())){
                String id;
                if(!TextUtils.isEmpty(form.getTitle())){
                    id = CommonUtil.getFirstPinyin(form.getTitle());
                }else{
                    id = "col"+i;
                }
                id = CommonUtil.getFormLastId(forms,id);
                forms.get(i).setId(id);
            }
        }
        LogUtils.e(JsonMananger.beanToJsonStr(forms));
        String code = "package com.gree.shyun.ui.orderdetail.com.gree.shyun.bean;\n\n";
        code+="import com.gree.shyun.ui.orderdetail.annotation.FormColumn;\n" +
                "import com.gree.shyun.ui.orderdetail.com.gree.shyun.entity.Form;\n" +
                "import com.gree.shyun.com.gree.shyun.bean.BaseBean;\n" +
                "import com.gree.shyun.server.db.annotation.Table;\n\n";
        code+="@Table\n";
        code+="public class "+formData.getClassName()+" extends BaseBean{\n";
        List<String> gets = new ArrayList<>();
        int i = 0 ;
        for(Form form:forms){
            code+="\t\t@FormColumn(";
            if(!TextUtils.isEmpty(form.getTitle())){
                code+="title=\""+form.getTitle()+"\",";
            }
            if(!TextUtils.isEmpty(form.getHint())){
                code+="hint=\""+form.getHint()+"\",";
            }

            code+="type="+getType(form.getType())+",";

            code+="enabled="+form.isEnabled()+",";
            code+="required="+form.isRequired()+",";

            if(!TextUtils.isEmpty(form.getDanw())){
                code+="danw=\""+form.getDanw()+"\",";
            }
            int len  = form.getLength();
            if(len==0){
                len = 300;
            }
            code+="length="+len+",";
            code+="order="+i+")\n";
            code+="\t\tprivate String "+form.getId()+";\n";
            String get = "\t\tpublic String get"+CommonUtil.firstToUpperCase(form.getId())+"(){ return "+form.getId()+";}\n";
            get+="\t\tpublic void set"+CommonUtil.firstToUpperCase(form.getId())+"(String "+form.getId()+"){ \n\t\t\tthis."+form.getId()+" = "+form.getId()+";\n";
            get+="\t\t}\n";
            gets.add(get);
            i++;
        }
        for(String s:gets){
            code+=s;
        }
        code+="}";
        return code;
    }

    private static String getType(int type){
        if(type==Form.TYPE_DEFAULT){
            return "Form.TYPE_DEFAULT";
        }else if(type==Form.TYPE_SELECT){
            return "Form.TYPE_SELECT";
        }else if(type==Form.TYPE_SELECT_DATE){
            return "Form.TYPE_SELECT_DATE";
        }else if(type==Form.TYPE_TEXT){
            return "Form.TYPE_TEXT";
        }else if(type==Form.TYPE_RADIO){
            return "Form.TYPE_RADIO";
        }else if(type==Form.TYPE_TITLE){
            return "Form.TYPE_TITLE";
        }else if(type==Form.TYPE_IMAGE){
            return "Form.TYPE_IMAGE";
        }else if(type==Form.TYPE_VIDEO){
            return "Form.TYPE_VIDEO";
        }else if(type==Form.TYPE_LINE){
            return "Form.TYPE_LINE";
        }else if(type==Form.TYPE_SELECT_BTN){
            return "Form.TYPE_SELECT_BTN";
        }else if(type==Form.TYPE_SWITCH){
            return "Form.TYPE_SWITCH";
        }else if(type==Form.TYPE_BUTTONS){
            return "Form.TYPE_BUTTONS";
        }else if(type==Form.TYPE_INPUT_SELECT_BTN){
            return "Form.TYPE_INPUT_SELECT_BTN";
        }else if(type==Form.TYPE_NUMBER_INPUT){
            return "Form.TYPE_NUMBER_INPUT";
        }else if(type==Form.TYPE_IMAGE_TEXT) {
            return "Form.TYPE_IMAGE_TEXT";
        }else if(type==Form.TYPE_MULTIPLE_SELECT) {
            return "Form.TYPE_MULTIPLE_SELECT";
        }else if(type==Form.TYPE_SELECT_DEL_BTN) {
            return "Form.TYPE_SELECT_DEL_BTN";
        }else if(type==Form.TYPE_CHECKBOX) {
            return "Form.TYPE_CHECKBOX";
        }else if(type==Form.TYPE_PASSWORD_INPUT) {
            return "Form.TYPE_PASSWORD_INPUT";
        }
        return "Form.TYPE_DEFAULT";
    }
}
