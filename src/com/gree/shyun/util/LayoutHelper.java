package com.gree.shyun.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LayoutHelper {
    public static List<String> getXmlns(String text){
        Pattern p=Pattern.compile("(xmlns\\:(.+)\")|(tools\\:(.+)\")");
        Matcher m=p.matcher(text);
        List<String> list = new ArrayList<>();
        while(m.find()) {
            String trigger=m.group(0);
            list.add(trigger);
        }
        return list;
    }
}
