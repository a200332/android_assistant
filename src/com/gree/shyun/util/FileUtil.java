package com.gree.shyun.util;

import com.gree.shyun.server.db.util.TextUtils;

import java.io.*;

public class FileUtil {


    /**
     * 获取资源根目录
     * @return
     */
    public static String getResRootPath(){
       return FileUtil.class.getResource("/res/").getPath().substring(1);
    }

    /**
     * 删除文件
     * @param path
     * @return
     */
    public static boolean removeFile(String path){
        if(!TextUtils.isEmpty(path)){
            File file = new File(path);
            if(file.exists()){
                return file.delete();
            }
        }
        return false;
    }

    /**
     * 删除文件夹
     * @param path
     * @return
     */
    public static void removeDir(String path){
        File file = new File(path);
        if(!file.exists()){
            return;
        }
        //文件名称列表
        String[] filePath = file.list();
        if(filePath==null){
            return;
        }
        for (int i = 0; i < filePath.length; i++) {
            File temp =new File(path + file.separator + filePath[i]);
            if (temp.isDirectory()) {
                removeDir(path  + file.separator  + filePath[i]);
            }
            temp.delete();
        }
        file.delete();
    }

    /**
     * 获取文件名
     * @param path
     * @return
     */
    public static String getFilename(String path){
        return path.substring(path.lastIndexOf("\\")+1);
    }

    /**
     * 获取文件夹数量
     * @param path
     * @return
     */
    public static int getFileCount(String path){
        File file = new File(path);
        if(file.exists()){
            return  file.list().length;
        }
        return 0;
    }
    /**
     * 判断文件是否存在
     * @param path
     * @return
     */
    public static boolean checkFileExists(String path){
        if(path==null){
            return false;
        }
        File file = new File(path);
        return file.exists();
    }

    /**
     *  复制整个文件夹
     * @param oldPath
     * @param newPath
     */
    public static void copyDir(String oldPath, String newPath) throws IOException {
        File file = new File(oldPath);
        if(!file.exists()){
            return;
        }
        //文件名称列表
        String[] filePath = file.list();
        if(filePath==null){
            return;
        }
        if (!(new File(newPath)).exists()) {
            (new File(newPath)).mkdir();
        }
        for (int i = 0; i < filePath.length; i++) {
            if ((new File(oldPath + file.separator + filePath[i])).isDirectory()) {
                copyDir(oldPath  + file.separator  + filePath[i], newPath  + file.separator + filePath[i]);
            }

            if (new File(oldPath  + file.separator + filePath[i]).isFile()) {
                copyFile(oldPath + file.separator + filePath[i], newPath + file.separator + filePath[i]);
            }
        }
    }

    /**
     * 复制文件
     * @param oldPath
     * @param newPath
     * @throws IOException
     */
    public static void copyFile(String oldPath, String newPath) throws IOException {
        File oldFile = new File(oldPath);
        File file = new File(newPath);
        FileInputStream in = new FileInputStream(oldFile);
        FileOutputStream out = new FileOutputStream(file);
        byte[] buffer=new byte[2097152];
        while((in.read(buffer)) != -1){
            out.write(buffer);
        }
    }
}
