package com.gree.shyun.entity;

import java.util.ArrayList;
import java.util.List;

public class FormData {
    private List<Form> forms = new ArrayList<>();
    private String className;

    public List<Form> getForms() {
        return forms;
    }

    public void setForms(List<Form> forms) {
        this.forms = forms;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
