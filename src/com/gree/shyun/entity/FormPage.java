package com.gree.shyun.entity;

public class FormPage {
    private String page_id;
    private String page_title;
    private String page_class;
    private String page_column;

    public String getPage_id() {
        return page_id;
    }

    public void setPage_id(String page_id) {
        this.page_id = page_id;
    }

    public String getPage_title() {
        return page_title;
    }

    public void setPage_title(String page_title) {
        this.page_title = page_title;
    }

    public String getPage_class() {
        return page_class;
    }

    public void setPage_class(String page_class) {
        this.page_class = page_class;
    }

    public String getPage_column() {
        return page_column;
    }

    public void setPage_column(String page_column) {
        this.page_column = page_column;
    }
}
