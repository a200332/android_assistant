package com.gree.shyun.entity;


import com.gree.shyun.server.db.annotation.Column;
import com.gree.shyun.server.db.annotation.Id;
import com.gree.shyun.server.db.annotation.NoAutoIncrement;

import java.util.Date;


public class Base {
    @Id
    @NoAutoIncrement
    @Column(desc = "ID")
    private String id ;
    @Column(desc = "备注")
    private String remark;
    @Column(desc = "创建时间")
    private Date createDate;
    @Column(desc = "更新时间")
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        if(updateDate==null){
            updateDate = new Date();
        }
        this.updateDate = updateDate;
    }
}
