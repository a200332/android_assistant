package com.gree.shyun.entity;

import com.gree.shyun.server.db.annotation.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * api分组
 */
@Table
public class ApiGroup extends Base{
    private String domain;//根
    private String domainMd5;//根签名
    private String name ;
    private List<ApiAction> apiActions = new ArrayList<>();
    private boolean isDel;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ApiAction> getApiActions() {
        return apiActions;
    }

    public void setApiActions(List<ApiAction> apiActions) {
        this.apiActions = apiActions;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomainMd5() {
        return domainMd5;
    }

    public void setDomainMd5(String domainMd5) {
        this.domainMd5 = domainMd5;
    }

    public boolean isDel() {
        return isDel;
    }

    public void setDel(boolean del) {
        isDel = del;
    }
}
