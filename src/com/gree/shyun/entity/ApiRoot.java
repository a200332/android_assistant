package com.gree.shyun.entity;

import com.gree.shyun.server.db.annotation.Table;

/**
 *  api 根路径
 */
@Table
public class ApiRoot extends  Base {
    private String key;
    private String url;//正式环境
    private String urlMd5;//正式环境
    private String testUrl;//测试环境
    private String testUrlMd5;//测试环境
    private String devUrl;//开发环境
    private String devUrlMd5;//开发环境

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTestUrl() {
        return testUrl;
    }

    public void setTestUrl(String testUrl) {
        this.testUrl = testUrl;
    }

    public String getDevUrl() {
        return devUrl;
    }

    public void setDevUrl(String devUrl) {
        this.devUrl = devUrl;
    }

    public String getUrlMd5() {
        return urlMd5;
    }

    public void setUrlMd5(String urlMd5) {
        this.urlMd5 = urlMd5;
    }

    public String getTestUrlMd5() {
        return testUrlMd5;
    }

    public void setTestUrlMd5(String testUrlMd5) {
        this.testUrlMd5 = testUrlMd5;
    }

    public String getDevUrlMd5() {
        return devUrlMd5;
    }

    public void setDevUrlMd5(String devUrlMd5) {
        this.devUrlMd5 = devUrlMd5;
    }
}
