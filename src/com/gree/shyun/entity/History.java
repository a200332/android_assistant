package com.gree.shyun.entity;


import com.gree.shyun.server.db.annotation.Table;

@Table
public class History extends Base {
    //键值不唯一  有多个 根据其组合生成ID
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
