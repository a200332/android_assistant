package com.gree.shyun.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class View {
    private String name;//显示名
    private String id;//id
    private String packName;//包名
    private List<View> child = new ArrayList<>();
    private Map<String,String> attribute = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if(!"root".equals(name)) {
            setPackName(getPackageName(name));
        }
    }
    private String  getPackageName(String name){
        if(name.contains(".")){
            return name;
        }else{
            return "android.widget."+name;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public List<View> getChild() {
        return child;
    }

    public void setChild(List<View> child) {
        this.child = child;
    }
    public void add(View view){
        child.add(view);
    }

    public void addAttribute(String key,String value){
        this.attribute.put(key,value);
    }
    public Map<String, String> getAttribute() {
        return attribute;
    }

    public void setAttribute(Map<String, String> attribute) {
        this.attribute = attribute;
    }
}
