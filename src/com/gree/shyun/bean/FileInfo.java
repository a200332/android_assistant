package com.gree.shyun.bean;

import java.util.List;

public class FileInfo {
    private String rootPath;//项目根路径
    private String filePath;//文件路径
    private String modelPath;//当前板块路径
    private String allPath;//全路径
    private List<XmlFile> layouts ;//界面

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getAllPath() {
        return allPath;
    }

    public void setAllPath(String allPath) {
        this.allPath = allPath;
    }

    public List<XmlFile> getLayouts() {
        return layouts;
    }

    public void setLayouts(List<XmlFile> layouts) {
        this.layouts = layouts;
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }
}
