package com.gree.shyun.bean;

/**
 * Create by 游德禄 on 2020-2-4
 * 反射实体 字段名 和类型
 */
public class BeanColumn {
    private String name ;
    private String type ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
