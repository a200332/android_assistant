package com.gree.shyun.gupiao.service;

import com.gree.shyun.gupiao.entity.FetchStep;
import com.gree.shyun.gupiao.task.GetGuPiaoMessageTask;
import com.gree.shyun.gupiao.task.GetGupiaoDataTask;
import com.gree.shyun.server.actions.QuoteList.respone.GpData;
import com.gree.shyun.server.task.TaskManager;
import com.gree.shyun.util.DateUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.gree.shyun.server.task.Task.SUCCESS;

public class FatchMessageImp implements FatchMessageDao {
    private List<GpData> gpData = null;
    private int currentIndex = 0;
    private int currentPageIndex = 0;

    /**
     * 获取当前股票信息
     * @return
     */
    private GpData getCurrent(){
        if(currentIndex<gpData.size()) {
            return gpData.get(currentIndex);
        }
        return null;
    }

    /**
     * 获取步骤信息
     * @param gpCode
     * @param pageIndex
     * @return
     */
    private FetchStep getFetchStep(String gpCode,int pageIndex){
        FetchStep fetchStep = new FetchStep();
        fetchStep.setStepId(UUID.randomUUID().toString());
        fetchStep.setCreateDate(new Date());
        fetchStep.setGpCode(gpCode);
        fetchStep.setPageIndex(pageIndex);
        return fetchStep;
    }

    /**
     * 是否有下一页
     * @param has
     */
    @Override
    public void hasNextPage(boolean has) {
        if(has){
            //如果有 当前页数增加1页
            currentPageIndex++;
        }else{
            //如果没有,股票数据的下标后移一位
            currentIndex++;
        }
    }

    /**
     * 启动程序
     */
    @Override
    public void start() {
        currentPageIndex=1;//当前页重置
        currentIndex = 0;//当前下标重置
        //去加载股票数据
        GetGupiaoDataTask task = new GetGupiaoDataTask();
        task.set("onCall",this);
        TaskManager.getInstance().exec(task, t -> {
            gpData = t.getResult();
            FatchMessageDao dao = t.getParam("onCall");
            if(gpData!=null&&gpData.size()>0){
                dao.onNext();
            }else{
                dao.onResult(false,TYPE_LOAD_GUPIAO,"加载股票数据失败");
            }
        });
    }

    /**
     * 执行下一步
     */
    @Override
    public void onNext() {
         GpData gpData = getCurrent();
         if(gpData!=null){
             //创建步骤信息
            FetchStep fetchStep = getFetchStep(gpData.getSymbol(),currentPageIndex);
            //根据步骤信息，去执行加载股票公告的请求
             GetGuPiaoMessageTask task = new GetGuPiaoMessageTask();
             task.set("fetchStep",fetchStep);
             task.set("onCall",this);
             TaskManager.getInstance().exec(task,t->{
                 FetchStep step = t.getParam("fetchStep");
                 FatchMessageDao dao = t.getParam("onCall");
                 if(step.getStatus()==SUCCESS){
                     //检查是否有下一页了
                     boolean hasNextPage = t.getParam("hasNextPage");
                     //判断是否需要翻页
                     dao.hasNextPage(hasNextPage);
                     //继续执行
                     try {
                         System.gc();
                         Thread.sleep(1000);
                     }catch (Exception e){

                     }
                     dao.onNext();
                 }else{
                     //如果出错
                     //翻译执行
                     dao.hasNextPage(true);
                     dao.onNext();
                     dao.onResult(false,TYPE_LOAD_MESSAGE,"加载公告出错:"+step.getErrorMessage());
                 }
             });
         }else{
             //股票数据全部遍历完成
             //统计执行成功和失败的的情况

             //拉取上次上传失败的数据，再扫描一次
             //等待5分钟,再次去扫描

             try {
                 Thread.sleep(5*60*1000);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }
    }


    private static  final  int TYPE_LOAD_GUPIAO = 0;//加载股票数据
    private static final int TYPE_LOAD_MESSAGE = 1;//加载公告数据

    /**
     * 统一处理结果
     * @param isSuccess
     * @param type
     * @param message
     */
    @Override
    public void onResult(boolean isSuccess,int type,String message) {
        if(isSuccess){
            switch (type){
                case TYPE_LOAD_GUPIAO:
                    break;
            }
        }else{
            switch (type){
                case TYPE_LOAD_GUPIAO:
                    break;
            }
        }
        print(message);
    }

    /**
     * 打印日志信息
     * @param msg
     */
    private void print(String msg){
        System.out.println("["+ DateUtil.format(new Date(),null)+"]:"+msg);
    }
}
