package com.gree.shyun.gupiao.service;

public interface FatchMessageDao {
    void start();//启动
    void onNext();//下一步
    void hasNextPage(boolean has);//是否有下一页了
    void onResult(boolean isSuccess,int type,String message);//执行的结果
}
