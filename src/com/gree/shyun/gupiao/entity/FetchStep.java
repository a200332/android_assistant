package com.gree.shyun.gupiao.entity;

import com.gree.shyun.server.db.annotation.Column;
import com.gree.shyun.server.db.annotation.Id;
import com.gree.shyun.server.db.annotation.NoAutoIncrement;
import com.gree.shyun.server.db.annotation.Table;

import java.util.Date;

@Table
public class FetchStep {

    @Id
    @NoAutoIncrement
    private String stepId;
    private String gpCode;
    private int pageIndex;
    private int status;//0 成功 - 1 失败
    private String errorMessage;//失败原因
    private Date CreateDate;

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date createDate) {
        CreateDate = createDate;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getGpCode() {
        return gpCode;
    }

    public void setGpCode(String gpCode) {
        this.gpCode = gpCode;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
