package com.gree.shyun.gupiao.entity;

import com.gree.shyun.server.db.annotation.Id;
import com.gree.shyun.server.db.annotation.NoAutoIncrement;
import com.gree.shyun.server.db.annotation.Table;

@Table
public class NewMessage {
    @Id
    @NoAutoIncrement
    private String id;
    private String messageId;
    private boolean hasRead;

    private boolean hasSend;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean isHasRead() {
        return hasRead;
    }

    public void setHasRead(boolean hasRead) {
        this.hasRead = hasRead;
    }

    public boolean isHasSend() {
        return hasSend;
    }

    public void setHasSend(boolean hasSend) {
        this.hasSend = hasSend;
    }
}
