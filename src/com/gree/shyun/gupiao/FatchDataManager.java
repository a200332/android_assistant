package com.gree.shyun.gupiao;

import com.gree.shyun.gupiao.service.FatchMessageDao;
import com.gree.shyun.gupiao.service.FatchMessageImp;
import com.gree.shyun.server.actions.QuoteList.respone.GpData;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取消息的执行器
 */
public class FatchDataManager{
    private List<GpData> gpDatas = new ArrayList<>();
    private static FatchDataManager fatchMessage;
    private FatchMessageDao fatchMessageDo;
    public static FatchDataManager getInstance(){
        if(fatchMessage==null){
            synchronized (FatchDataManager.class){
                if(fatchMessage==null){
                    fatchMessage = new FatchDataManager();
                    fatchMessage.init();
                }
            }
        }
        return  fatchMessage;
    }

    /**
     * 初始化
     */
    private void init(){
        fatchMessageDo = new FatchMessageImp();
    }

    /**
     * 启动
     */
    public void start(){
        fatchMessageDo.start();
    }
}
