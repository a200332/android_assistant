package com.gree.shyun.gupiao.task;

import com.gree.shyun.gupiao.entity.FetchStep;
import com.gree.shyun.gupiao.entity.NewMessage;
import com.gree.shyun.server.actions.InfoSearch.InSearchAction;
import com.gree.shyun.server.actions.InfoSearch.requestParam.InSearchParam;
import com.gree.shyun.server.actions.InfoSearch.respone.InSearchRespone;
import com.gree.shyun.server.actions.InfoSearch.respone.MessageInfo;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.util.CommonUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class GetGuPiaoMessageTask extends Task {
    @Override
    public Task doTask() {
        FetchStep fetchStep = getParam("fetchStep");



        try {
            InSearchParam inSearchParam = new InSearchParam();
            inSearchParam.setSymbol_id(fetchStep.getGpCode());//sh600077,
            inSearchParam.setPage(fetchStep.getPageIndex());//2,
            inSearchParam.setCount(50);
            inSearchParam.setSource("公告");

            InSearchAction action= new InSearchAction();
            InSearchRespone respone = action.get(inSearchParam);

            float progress = ((float)fetchStep.getPageIndex()/respone.getMaxPage())*100;
           System.out.println(fetchStep.getGpCode()+"->第["+fetchStep.getPageIndex()+"/"+respone.getMaxPage()+"]页，进度:"+progress+"%");
            if(respone.getList()!=null&&fetchStep.getPageIndex()<respone.getMaxPage()){
                List<MessageInfo> gpMessages = respone.getList();
                boolean hasNext = true;
                if(gpMessages!=null&&gpMessages.size()>0) {
                    for (MessageInfo gpMessage : gpMessages) {
                        long time = gpMessage.getCreated_at();
                        Date date = new Date(time);
                        Date d = new Date(new Date().getTime() - (3*24*60*60*1000));
                        if(date.before(d)){
                            hasNext = false;
                        }
                        MessageInfo gpMes = DbHelper.findById(MessageInfo.class, gpMessage.getId());
                        if (gpMes != null) {
                            System.out.println("存在:"+gpMes.getTitle());
                            hasNext = false;
                        }else{
                            //这里是新发现的，记录到待发送的表里
                            NewMessage newMessage = new NewMessage();
                            newMessage.setId(UUID.randomUUID().toString());
                            newMessage.setMessageId(gpMessage.getId());
                            DbHelper.save(newMessage);
                        }
                        gpMessage.setTitle(CommonUtil.unicodeToUtf8(gpMessage.getTitle()));
                    }
                    DbHelper.saveOrUpdate(gpMessages);
                }else {
                    hasNext = false;
                }
                set("hasNextPage",hasNext);
            }else{
                set("hasNextPage",false);
            }
            fetchStep.setStatus(SUCCESS);
        }catch (Exception e){
            fetchStep.setStatus(ERROR);
            fetchStep.setErrorMessage(e.getMessage());
        }
        set("fetchStep",fetchStep);
        DbHelper.saveOrUpdate(fetchStep);
        return this;
    }
}
