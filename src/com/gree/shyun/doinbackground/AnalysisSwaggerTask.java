package com.gree.shyun.doinbackground;

import com.gree.shyun.server.db.util.TextUtils;
import com.gree.shyun.server.swagger.AnalysisSwagger;
import com.gree.shyun.server.taskpool.async.bean.Param;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.IWorker;
import com.gree.shyun.server.taskpool.async.wrapper.WorkerWrapper;
import com.gree.shyun.service.ProcessService;

import java.util.Map;

public class AnalysisSwaggerTask implements IWorker<Param, Result> {

    @Override
    public Result action(Param param, Map<String, WorkerWrapper> allWrappers) {
        String method = param.get();
        Object data = param.get("data");
        if (!"analysisSwagger".equals(method)) {
            return ProcessService.error(method, 0, "非analysisSwagger接口!");
        }
        if(onCallback==null){
            return ProcessService.error(method, 0, "回调为空!");
        }
        String json = data.toString();
        if(TextUtils.isEmpty(json)) {
            return ProcessService.error(method, 0, "数据为空!");
        }
        AnalysisSwagger.toAnalysisSwagger(json,onCallback);
        return null;
    }
    private AnalysisSwagger.OnCallback onCallback;

    public AnalysisSwagger.OnCallback getOnCallback() {
        return onCallback;
    }

    public void setOnCallback(AnalysisSwagger.OnCallback onCallback) {
        this.onCallback = onCallback;
    }
}
