package com.gree.shyun.doinbackground;

import com.gree.shyun.server.actions.QuoteList.respone.GpData;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.util.FileUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ToPdfTask extends Task {
    @Override
    public Task doTask() {
        List<GpData> gpData = DbHelper.findAll(GpData.class);
        String style = "<style>" +
                ".img{float:left;width:240px;}" +
                "</style>";
        String html=style+"<div class='imgbox'>";
        for(GpData gp:gpData){
            html+="<img src=\"http://image.sinajs.cn/newchart/monthly/n/"+gp.getSymbol().toLowerCase()+".gif\" class=\"img\"/>";
        }
        html+="</div>";
        File file = new File(FileUtil.getResRootPath()+"pdf/gupiao.html");

        System.out.println(file.getAbsolutePath());
        try {
            FileUtils.writeStringToFile(file,html);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }
}
