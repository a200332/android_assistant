package com.gree.shyun.doinbackground;

import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.db.sqlite.Selector;
import com.gree.shyun.server.db.util.TextUtils;
import com.gree.shyun.server.generatingcode.GeneratingJava;
import com.gree.shyun.server.taskpool.async.bean.Param;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.IWorker;
import com.gree.shyun.server.taskpool.async.wrapper.WorkerWrapper;
import com.gree.shyun.entity.ApiAction;
import com.gree.shyun.service.ProcessService;

import java.util.List;
import java.util.Map;

public class GeneratingJavaTask implements IWorker<Param, Result> {

    @Override
    public Result action(Param param, Map<String, WorkerWrapper> allWrappers) {
        String method = param.get();
        Object data = param.get("data");
        String resRoot = param.get("resRoot");
        if (!"generateCode".equals(method)) {
            return ProcessService.error(method, 0, "非generateCode接口!");
        }
        if(onCallback==null){
            return ProcessService.error(method, 0, "回调为空!");
        }
        String id = data.toString();
        if(!TextUtils.isEmpty(id)){
            ApiAction apiAction = DbHelper.findById(ApiAction.class,id);
            if(apiAction==null){
                return ProcessService.error(method,-1,"不存在此数据");
            }
             GeneratingJava.generating(resRoot,apiAction,onCallback);
        }else{
            List<ApiAction> allApis = DbHelper.findAll(Selector.from(ApiAction.class).where("isDel","=",false));
            GeneratingJava.generating(resRoot,allApis,onCallback);
        }
        return null;
    }
    private GeneratingJava.OnCallback onCallback;

    public GeneratingJava.OnCallback getOnCallback() {
        return onCallback;
    }

    public void setOnCallback(GeneratingJava.OnCallback onCallback) {
        this.onCallback = onCallback;
    }
}
