package com.gree.shyun.doinbackground;

import com.gree.shyun.server.swagger.AnalysisSwagger;
import com.gree.shyun.server.taskpool.async.bean.Param;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.IWorker;
import com.gree.shyun.server.taskpool.async.wrapper.WorkerWrapper;

import java.util.Map;

public class ExecLayoutToVMTask implements IWorker<Param, Result> {

    @Override
    public Result action(Param param, Map<String, WorkerWrapper> allWrappers) {
        String method = param.get();
        return null;
    }
    private AnalysisSwagger.OnCallback onCallback;

    public AnalysisSwagger.OnCallback getOnCallback() {
        return onCallback;
    }

    public void setOnCallback(AnalysisSwagger.OnCallback onCallback) {
        this.onCallback = onCallback;
    }
}
