package com.gree.shyun.server.mail;

/**
 * 邮件Subject模型
 */
public class EmailModel {

    //======
    //构造函数
    //======
    public EmailModel() {}

    public EmailModel(String mailTo) {
        this.mailTo = mailTo;
    }

    public EmailModel(String subjectTitle, String subjectContxt, String[] filePath,String mailTo) {
        this.subjectTitle = subjectTitle;
        this.subjectContxt = subjectContxt;
        this.filePath = filePath;
        this.mailTo = mailTo;
    }

    //==========
    //构造函数end
    //==========

    private String subjectTitle;

    private String subjectContxt;

    private String mailFrom ;

    private String mailTo;

    private String subjectHtmlContxt = "";

    private String[] filePath;//文件路径

    public String getSubjectTitle() {
        return subjectTitle;
    }

    public void setSubjectTitle(String subjectTitle) {
        this.subjectTitle = subjectTitle;
    }

    public String getSubjectContxt() {
        return subjectContxt;
    }

    public void setSubjectContxt(String subjectContxt) {
        this.subjectContxt = subjectContxt;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getSubjectHtmlContxt() {
        return subjectHtmlContxt;
    }

    public void setSubjectHtmlContxt(String subjectHtmlContxt) {
        this.subjectHtmlContxt = subjectHtmlContxt;
    }

    public String[] getFilePath() {
        return filePath;
    }

    public void setFilePath(String[] filePath) {
        this.filePath = filePath;
    }
}
