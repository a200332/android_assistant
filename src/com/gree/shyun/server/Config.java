package com.gree.shyun.server;

import com.gree.shyun.autopage.bean.Item;
import com.gree.shyun.entity.ApiAction;
import com.gree.shyun.entity.ApiGroup;
import com.gree.shyun.entity.ApiRoot;
import com.gree.shyun.entity.History;
import com.gree.shyun.server.actions.QuoteList.respone.GpData;

public class Config {
    public final static String dbName = "data.db";
    public final static boolean debug = false;
    public final static int dbVersion = 12;
    //所有表配置
    public static  Class[] table = {
            History.class,
            ApiAction.class,
            ApiGroup.class,
            ApiRoot.class,
            GpData.class,
            Item.class
    };

    /**
     * 初始化数据
     */
    public static void initData(){


    }
}
