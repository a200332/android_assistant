package com.gree.shyun.server.gupiao.bean;

import com.gree.shyun.server.db.annotation.Table;
import com.gree.shyun.entity.Base;

/**
 * 实现目的
 * 1、监听所有急涨急跌 跌倒启动点位置股
 * 2、监听所有减持公告股
 * 3、监听所有相对低价股
 * 4、监听所有消息面，并生成A4打印出来
 *
 *  关系图：
 *    股票
 *
 *
 */
@Table
public class GuPiao  extends Base {
    private String name;//名称
    private String code;//代码

}
