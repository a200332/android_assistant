package com.gree.shyun.server.gupiao.bean;

import com.gree.shyun.server.db.annotation.Table;
import com.gree.shyun.entity.Base;

import java.util.List;

/**
 * 消息
 */
@Table
public class GuPiaoMessage extends Base {
    private String message;//消息内容
    private String importance;//重要性
    private String date;//日期
    private String time;//时间
    private String type;//类型
    private List<String> somePiao;//相关票

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getSomePiao() {
        return somePiao;
    }

    public void setSomePiao(List<String> somePiao) {
        this.somePiao = somePiao;
    }
}
