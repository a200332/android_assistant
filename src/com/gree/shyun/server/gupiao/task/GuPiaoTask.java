package com.gree.shyun.server.gupiao.task;

import com.gree.shyun.server.gupiao.service.GuPiaoService;
import com.gree.shyun.server.taskpool.async.bean.Param;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.IWorker;
import com.gree.shyun.server.taskpool.async.wrapper.WorkerWrapper;
import com.gree.shyun.service.ProcessService;

import java.util.Map;

public class GuPiaoTask implements IWorker<Param, Result> {

    @Override
    public Result action(Param param, Map<String, WorkerWrapper> allWrappers) {
        String method = param.get();
        Object data = param.get("data");
        String resRoot = param.get("resRoot");
        try {
                GuPiaoService guPiaoService = new GuPiaoService();
            return  guPiaoService.handle(method,data,resRoot);
        }catch (Exception e){
            return ProcessService.error(method,500,"执行出错:"+e.getMessage());
        }
    }
}
