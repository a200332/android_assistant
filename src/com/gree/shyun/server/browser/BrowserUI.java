package com.gree.shyun.server.browser;

import com.gree.shyun.bean.AddIcon;
import com.gree.shyun.bean.Message;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.db.util.LogUtils;
import com.gree.shyun.server.db.util.TextUtils;
import com.gree.shyun.server.generatingcode.GeneratingJava;
import com.gree.shyun.server.gupiao.task.GuPiaoTask;
import com.gree.shyun.server.swagger.AnalysisSwagger;
import com.gree.shyun.server.taskpool.TaskPool;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.Callback;
import com.gree.shyun.server.taskpool.async.worker.ResultState;
import com.gree.shyun.server.taskpool.async.worker.WorkResult;
import com.gree.shyun.doinbackground.*;
import com.gree.shyun.entity.History;
import com.gree.shyun.util.FileUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import com.gree.shyun.service.ProcessService;
import com.gree.shyun.util.CheckFileConsistency;
import com.gree.shyun.util.ImageUtil;
import com.gree.shyun.util.MD5;
import com.gree.shyun.util.json.JsonMananger;
import org.eclipse.swt.widgets.Shell;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class BrowserUI extends BrowserFunction {

    public static String resRoot = FileUtil.getResRootPath();
    private boolean isCompleted = false;
    public BrowserUI(Browser browser, String name,String index) {
        super(browser, name);
        String rootPath = resRoot+(index==null?"index.html":index);
        browser.setUrl(rootPath);
        browser.addProgressListener(new ProgressListener() {
            @Override
            public void changed(ProgressEvent progressEvent) {

            }

            @Override
            public void completed(ProgressEvent progressEvent) {
                isCompleted = true;
                callback("onReady","onReady");
            }
        });
        init();
    }
    //配置信息
    public static Map<String,String> config;

    /**
     * 初始化
     */
    private void init(){
        TaskPool.exec(InitDbTask.class).addParam(resRoot).callback(new Callback() {
            @Override
            public void result(boolean success, WorkResult<Result> result) {
                if(success){
                    config = result.getResult().get();
                }
            }
        }).start();
    }

    @Override
    public Object function(Object[] arguments) {
        if(arguments.length>=2){
            call(arguments[0].toString(),arguments[1]);
        }
        return super.function(arguments);
    }

    /**
     * 调用方法
     * @param method
     * @param data
     */
    private void call(String method,Object data){
        String prefix = "gupiao_";
        if(method.startsWith(prefix)){
            exec(GuPiaoTask.class,method.substring(prefix.length()),data);
            return;
        }
        //这个方法直接去执行task
        if(method.startsWith("exec_")){
            //exec_autopage.task.GetPageTask
            String className ="com.gree.shyun."+ method.substring(method.indexOf("_")+1);
            exec(className,method,data);
            return;
        }
        switch (method){
            case "openSourcePath":
                try {
                    Desktop.getDesktop().open(new File(resRoot+"server/actions/"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "openFile":
                FileDialog fd=new FileDialog(shell, SWT.OPEN);
                History history = DbHelper.findById(History.class,MD5.encrypt(method));
                if(history!=null){
                    fd.setFilterPath(history.getValue());
                }else{
                    fd.setFilterPath(System.getProperty("user.dir"));
                }
                fd.setFilterExtensions(new String[]{"*.saz"});
                fd.setFilterNames(new String[]{"Fiddler Session Archive(*.saz)"});
                String file=fd.open();
                if(file!=null){
                    errorBack(method,0,"正在解析数据...");
                    history = new History();
                    history.setId(MD5.encrypt(method));
                    history.setKey(method);
                    File file1 = new File(file);
                    history.setValue(file1.getParent().replaceAll("\\\\","/"));
                    DbHelper.saveOrUpdate(history);
                    call("analysisFiddler",file);
                }
                break;
            case "layoutToVM":
                FileDialog layoutToVM=new FileDialog(shell, SWT.OPEN);
                History layoutToVMHistory = DbHelper.findById(History.class,MD5.encrypt(method));
                if(layoutToVMHistory!=null){
                    layoutToVM.setFilterPath(layoutToVMHistory.getValue());
                }else{
                    layoutToVM.setFilterPath(System.getProperty("user.dir"));
                }
                layoutToVM.setFilterExtensions(new String[]{"*.xml"});
                layoutToVM.setFilterNames(new String[]{"安卓布局(*.xml)"});
                String layoutToVMPath=layoutToVM.open();
                if(layoutToVMPath!=null){
                    File f = new File(layoutToVMPath);
                    if(f.exists()){
                        exec(BrowserTask.class,"ExecLayoutToVM",f.getPath());
                        layoutToVMHistory = new History();
                        layoutToVMHistory.setId(MD5.encrypt(method));
                        layoutToVMHistory.setKey(method);
                        layoutToVMHistory.setValue(f.getParent().replaceAll("\\\\","/"));
                        DbHelper.saveOrUpdate(layoutToVMHistory);
                    }
                }
                errorBack(method,0,"请选择文件...");
                break;
            case "activityToVM":
                FileDialog activityToVM=new FileDialog(shell, SWT.OPEN);
                History activityToVMHistory = DbHelper.findById(History.class,MD5.encrypt(method));
                if(activityToVMHistory!=null){
                    activityToVM.setFilterPath(activityToVMHistory.getValue());
                }else{
                    activityToVM.setFilterPath(System.getProperty("user.dir"));
                }
                activityToVM.setFilterExtensions(new String[]{"*.java"});
                activityToVM.setFilterNames(new String[]{"java文件(*.java)"});
                String activityToVMPath=activityToVM.open();
                if(activityToVMPath!=null){
                    File f = new File(activityToVMPath);
                    if(f.exists()){
                        exec(BrowserTask.class,"ExecActivityToVM",f.getPath());
                        activityToVMHistory = new History();
                        activityToVMHistory.setId(MD5.encrypt(method));
                        activityToVMHistory.setKey(method);
                        activityToVMHistory.setValue(f.getParent().replaceAll("\\\\","/"));
                        DbHelper.saveOrUpdate(activityToVMHistory);
                    }
                }
                errorBack(method,0,"请选择文件...");
                break;
            case "fileSign":
                FileDialog fileSign=new FileDialog(shell, SWT.OPEN);
                History fileSignHistory = DbHelper.findById(History.class,MD5.encrypt(method));
                if(fileSignHistory!=null){
                    fileSign.setFilterPath(fileSignHistory.getValue());
                }else{
                    fileSign.setFilterPath(System.getProperty("user.dir"));
                }
                fileSign.setFilterExtensions(new String[]{"*.apk"});
                fileSign.setFilterNames(new String[]{"安卓应用(*.apk)"});
                String fileSignPath=fileSign.open();
                if(fileSignPath!=null){
                    File f = new File(fileSignPath);
                    if(f.exists()){
                        try {
                            String sign = CheckFileConsistency.getFileMD5String(f);
                            successBack(method,sign);
                            fileSignHistory = new History();
                            fileSignHistory.setId(MD5.encrypt(method));
                            fileSignHistory.setKey(method);
                            fileSignHistory.setValue(f.getParent().replaceAll("\\\\","/"));
                            DbHelper.saveOrUpdate(fileSignHistory);
                            return;
                        } catch (IOException e) {
                            e.printStackTrace();
                            errorBack(method,0,"文件签名失败！");
                            return;
                        }
                    }
                }
                errorBack(method,0,"请选择文件...");
                break;
            case "addIcon":
                if(data==null){
                    errorBack(method,400,"参数为空!");
                    return;
                }
                AddIcon addIcon = JsonMananger.jsonToBean(data.toString(),AddIcon.class);
                //可多选
                FileDialog addIconDialog=new FileDialog(shell, SWT.OPEN|SWT.MULTI);
                History addIconHistory = DbHelper.findById(History.class,MD5.encrypt(method));
                if(addIconHistory!=null){
                    addIconDialog.setFilterPath(addIconHistory.getValue());
                }else{
                    addIconDialog.setFilterPath(System.getProperty("user.dir"));
                }
                addIconDialog.setFilterExtensions(new String[]{"*.png"});
                addIconDialog.setFilterNames(new String[]{"PNG图片[大于200*200像素](*.png)"});
                String iconFile = addIconDialog.open();
                if(ProcessService.fileInfo==null){
                    errorBack(method,0,"android studio 未打开项目");
                    return;
                }

                if(iconFile==null){
                    errorBack(method,0,"未选择文件...");
                    return;
                }
                File icFile = new File(iconFile);
                String [] iconList=addIconDialog.getFileNames();//这里是获取文件文件的名字 字符数组

                String parent = icFile.getParent().replaceAll("\\\\","/");;
                int i = 1 ;
                String filename = addIcon.getFilename();
                String result = "";
                for(String addIconPath:iconList){
                        File f = new File(parent+"/"+addIconPath);
                        if(f.exists()){
                            try {
                                if(!ImageUtil.check(f)){
                                    result+="图片"+i+"长宽需大于200像素;";
                                    continue;
                                }
                                if(!TextUtils.isEmpty(filename)&&iconFile.length()>1){
                                    addIcon.setFilename(filename+i);
                                }
                                boolean success = ImageUtil.zoomImages(f,ProcessService.fileInfo.getModelPath(),addIcon);
                                if(success){
                                    result+="图片"+i+"添加成功;";
                                }else{
                                    result+="图片"+i+"添加失败;";
                                }
                            } catch (IOException e) {
                                result+="图片"+i+"裁剪失败,请重试!错误信息:"+e.getMessage()+";";
                            }
                    }
                    i++;
                }
                if(parent!=null) {
                    addIconHistory = new History();
                    addIconHistory.setId(MD5.encrypt(method));
                    addIconHistory.setKey(method);
                    addIconHistory.setValue(parent);
                    DbHelper.saveOrUpdate(addIconHistory);
                }
                successBack(method,result);
              break;
            case "analysisSwagger":
                    AnalysisSwaggerTask analysisSwaggerTask = new AnalysisSwaggerTask();
                    analysisSwaggerTask.setOnCallback(new AnalysisSwagger.OnCallback() {
                        @Override
                        public void onProgress(int position, int max, String msg) {
                            errorBack(method,1,msg);
                        }

                        @Override
                        public void onSuccess(String msg) {
                            successBack(method,msg);
                        }

                        @Override
                        public void onError(String msg) {
                            errorBack(method,0,msg);
                        }
                    });
                    exec(analysisSwaggerTask,method,data);
                    break;
            case "generateCode":
                GeneratingJavaTask generatingJavaTask = new GeneratingJavaTask();
                generatingJavaTask.setOnCallback(new GeneratingJava.OnCallback() {
                    @Override
                    public void onProgress(int position, int size, String msg) {
                        errorBack(method,1,msg);
                    }

                    @Override
                    public void onSuccess(String msg) {
                        successBack(method,msg);
                    }

                    @Override
                    public void onError(String msg) {
                        errorBack(method,0,msg);
                    }
                });
                exec(generatingJavaTask,method,data);
                break;
            case "generateCode1":
                GeneratingJava1Task generatingJavaTask1 = new GeneratingJava1Task();
                generatingJavaTask1.setOnCallback(new GeneratingJava.OnCallback() {
                    @Override
                    public void onProgress(int position, int size, String msg) {
                        errorBack(method,1,msg);
                    }

                    @Override
                    public void onSuccess(String msg) {
                        successBack(method,msg);
                    }

                    @Override
                    public void onError(String msg) {
                        errorBack(method,0,msg);
                    }
                });
                exec(generatingJavaTask1,method,data);
                break;

            case "generateCode2":
                GeneratingJava2Task generatingJavaTask2 = new GeneratingJava2Task();
                generatingJavaTask2.setOnCallback(new GeneratingJava.OnCallback() {
                    @Override
                    public void onProgress(int position, int size, String msg) {
                        errorBack(method,1,msg);
                    }

                    @Override
                    public void onSuccess(String msg) {
                        successBack(method,msg);
                    }

                    @Override
                    public void onError(String msg) {
                        errorBack(method,0,msg);
                    }
                });
                exec(generatingJavaTask2,method,data);
                break;
                default:
                    exec(BrowserTask.class,method,data);
                    break;

        }
    }

    /**
     * 执行异步任务
     * @param obj
     * @param method
     * @param data
     */
    private void exec(Object obj,String method,Object data){
        TaskPool.exec(obj).addParam(method)
                .addParam("data",data)
                .addParam("resRoot",resRoot)
                .callback((success, result) -> {
                    if(result==null){
                        return;
                    }
                    String key = result.getResult().get();
                    if(key==null){
                        return;
                    }
                    if(success){
                        String data1 = result.getResult().get("result");
                        callback(key, data1);
                    }else{
                        if(result.getResultState()== ResultState.TIMEOUT){
                            errorBack(key,408,"执行超时!");
                        }else if(result.getResultState()==ResultState.EXCEPTION){
                            errorBack(key,503, "执行出错:"+result.getEx().getMessage());
                        }else{
                            errorBack(key,400,"发生未知错误");
                        }
                    }
                }).start();
    }

    /**
     *  回调
     * @param method
     * @param json
     */
    private void callback(String method,String json){
        if(isCompleted){
            try {
                Display.getDefault().asyncExec(new Runnable() {
                    @Override
                    public void run() {
                       boolean b = getBrowser().execute( "callback('"+method+"','"+json+"');");
                       if(!b){
                           LogUtils.e(method+"->回调失败。");
                       }
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 成功
     * @param method
     * @param msg
     */
    private void successBack(String method,String msg){
        errorBack(method,200,msg);
    }
    /**
     *  出错
     * @param method
     * @param code
     * @param msg
     */
    private void errorBack(String method,int code,String msg){
        Message message = new Message();
        message.setCode(code);
        if(code==200){
            message.setData(msg);
        }else{
            message.setMessage(msg);
        }
        callback(method, JsonMananger.beanToJsonStr(message));
    }


    /**
     * 创建界面
     */
    public static Shell shell;
    public static void create() {
        create(null,0,0,null);
    }
    public static void create(String index,int width,int height,String name) {
        Display display = new Display();
        shell = new Shell(display, SWT.MIN|SWT.MAX|SWT.CLOSE|SWT.RESIZE);
        shell.setLayout(new FillLayout());
        if(width==0) {
             width = display.getClientArea().width / 2;
        }
        if(height==0){
            height = display.getClientArea().height*2/3;
        }
        shell.setLocation(-10,display.getClientArea().height-height+10);
        shell.setSize(width,height);
        shell.setText(name==null?"Android辅助开发工具[作者：游德禄]":name);
        Browser browser = new Browser(shell, SWT.FILL_WINDING);
        new BrowserUI(browser,"exec",index);
        //设置图标
        org.eclipse.swt.graphics.Image image = new Image(shell.getDisplay(), BrowserUI.resRoot+"icon.ico");
        shell.setImage(image);
        shell.layout();
        shell.open();

        while (!shell.isDisposed()){
            if(!display.readAndDispatch()){
                display.sleep();
            }
        }
        display.dispose();
    }
}
