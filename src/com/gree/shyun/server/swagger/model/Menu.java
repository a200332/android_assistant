package com.gree.shyun.server.swagger.model;

/**
 */
public class Menu {


    /**
     * 标题
     */
    private String title;

    /**
     * 父级菜单id
     */
    private Integer pid;

    /**
     * 权限访问路径
     */
    private String url;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 菜单类型1-表示一级菜单2-表示子菜单3-表示非菜单
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}