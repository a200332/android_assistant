package com.gree.shyun.server.generatingcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ToBeanResult {
    private Set<String> table = new HashSet<>();
    private Map<String,String> beans = new HashMap<>();

    public Set<String> getTable() {
        return table;
    }

    public void setTable(Set<String> table) {
        this.table = table;
    }

    public Map<String, String> getBeans() {
        return beans;
    }

    public void setBeans(Map<String, String> beans) {
        this.beans = beans;
    }
}
