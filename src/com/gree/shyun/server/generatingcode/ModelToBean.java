package com.gree.shyun.server.generatingcode;

import com.gree.shyun.util.DateUtil;

import java.util.*;

public class ModelToBean {
    private static String username = "youdelu";
    /**
     * Model转JavaBean
     * @param packa
     */
    public static ToBeanResult generate(String txt,String packa){
        ToBeanResult toBeanResult = new ToBeanResult();
        //生成日期
        String datestr  = DateUtil.format(new Date(), null);
        //表
        Set<String> table = new HashSet<>();
        String[] txts = txt.split("\n");
        Map<String,String> beans = new HashMap<>();

        String temp = "";
        String className = "";
        String bean = "";
        String remark = "";
        String impList = "import java.util.List;";
        String impDate = "import java.util.Date;";
        String impId = "import com.lidroid.xutils.db.annotation.Id;";
        impId+="\n";
        impId+="import com.lidroid.xutils.db.annotation.NoAutoIncrement;";
        impId+="\n";
        impId+="import com.lidroid.xutils.db.annotation.Table;";
        boolean cTable = false ;
        boolean hasList = false;
        boolean hasId = false;
        boolean hasDate = false ;
        for(int i = 0 ; i < txts.length;i++){
            String line = txts[i];
            if(line.length()<1){
                continue;
            }
            if(line.startsWith("/")){
                line=line.replaceAll("/", "");
                remark+="* "+line+"\n";
                continue;
            }
            line = line.replaceAll(", optional", "");
            line = line.replaceAll("«", "");
            line = line.replaceAll("»", "");
            line = line.replaceAll("'", "\"");
            if(line.contains(":")){
                int index = line.indexOf(")");
                String tempRight = line.substring(index+2);
                line = line.substring(0,index+1).concat(";//");
                line =line.concat(tempRight);
                if(line.substring(line.length()-1).contains(",")){
                    line = line.substring(0,line.length()-1);
                }
            }else{
                String lastStr = line.substring(line.length()-1);
                if(lastStr.contains(",")){
                    line = line.substring(0,line.length()-1).concat(";");
                }else if(!lastStr.contains("{")&&!lastStr.contains("}")){
                    if(line.length()>1){
                        line+=";";
                    }
                }else if(lastStr.contains("{")){
                    String header = "package "+packa;
                    header+="\n";
                    header+="#import#";
                    header+="\n";
                    header+="/**";
                    header+="\n";
                    header+="* Created by "+username+" on "+datestr+".";
                    header+="\n";
                    header+=remark;
                    header+="*";
                    header+="\n";
                    header+="*/";
                    header+="\n";

                    remark="";
                    className = line.replaceAll("\\{", "");
                    className = className.replaceAll(" ", "");
                    String tab = className.toLowerCase();
                    if(tab.startsWith("@")){
                        tab = tab.replaceAll("@", "");
                        if(tab.contains("|")){
                            String zj = tab.substring(0, line.indexOf("|"));
                            header+="\n"+zj+"\n";
                            line = line.substring(line.indexOf("|")+1);
                            className = className.substring(className.indexOf("|")+1);
                        }else{
                            header+="\n@Table(name = \""+tab+"\")"+"\n";
                            line = line.replaceAll("@", "");
                            className = className.replaceAll("@", "");
                        }
                        cTable = true;
                    }


                    header+="public class ";

                    line = header+line;
                    line +="\n";
                }
            }
            if(line.length()>0&&line.contains("(")&&!line.contains("{")){
                String type = line.substring(line.indexOf("(")+1,line.indexOf(")"));
                if(type!=null){
                    String tempLine = line ;
                    line = tempLine.substring(0, tempLine.indexOf("("));
                    String propertie = line.replaceAll(" ", "");
                    String value = tempLine.substring(tempLine.indexOf(")")+1);
                    line += value;
                    type = type.replaceAll("string", "String");
                    type = type.replaceAll("object", "Object");
                    type = type.replaceAll("Datetime", "Date");
                    if(type.contains("Array")){
                        hasList = true ;
                        type = type.replaceAll("Array", "List");
                        type = type.replaceAll("integer", "Integer");
                        type = type.replaceAll("number", "Double");
                    }else{
                        type = type.replaceAll("integer", "int");
                        type = type.replaceAll("number", "double");
                    }
                    if(type.contains("List")){
                        hasList = true;
                    }
                    if(type.contains("[")){
                        String v = type.substring(type.indexOf("[")+1, type.indexOf("]"));
                        if(v.length()>0){
                            type = type.replaceAll("\\[", "<");
                            type = type.replaceAll("\\]", ">");
                        }
                    }

                    if(type.contains("Date")){
                        hasDate = true ;
                    }
                    if(value.contains("[")){
                        //type+="[]";
                        line = line.replaceAll("\\=", ";//可选数据");
                        line = line.replaceAll("\\[", "{");
                        line = line.replaceAll("\\]", "}");
                    }

                    String upperPropertie =propertie;
                    String pri ="	private ";
//			     		if(propertie.equals("id")){
//			     			hasId  = true ;
//			     			pri="	@Id"+"\n"+pri;
//			     		}
                    if(propertie.startsWith("@")){
                        propertie = propertie.replaceAll("@", "");
                        if(propertie.contains("|")){
                            String zj = propertie.substring(0, propertie.indexOf("|"));
                            pri="	"+zj+"\n"+pri;
                            line = line.substring(line.indexOf("|")+1,line.length());
                            propertie = propertie.substring(propertie.indexOf("|")+1,propertie.length());
                            upperPropertie = upperPropertie.substring(upperPropertie.indexOf("|")+1,upperPropertie.length());
                        }else{
                            pri="	@Id(column = \""+propertie+"\")\n	@NoAutoIncrement\n"+pri;
                            line = line.replaceAll("@", "");
                            upperPropertie = upperPropertie.replaceAll("@", "");

                        }
                        cTable = true;
                    }
                    upperPropertie =upperPropertie.substring(0,1).toUpperCase().concat(upperPropertie.substring(1));
                    line = pri +type+" "+line;
                    line +="\n";
                    //get set
                    temp +="\n";
                    temp += "	public void set"+upperPropertie+"("+type+" "+propertie+"){";
                    temp +="\n";
                    temp +="		this."+propertie+"="+propertie+";";
                    temp +="\n";
                    temp +="	}";
                    temp +="\n";
                    temp += "	public "+type+" get"+upperPropertie+"(){";
                    temp +="\n";
                    temp +="		return "+propertie+";";
                    temp +="\n";
                    temp +="	}";
                    temp +="\n";

                }
            }

            if(line.length()<=2){
                bean+=temp;
//		     		if(cTable&&!hasId){
//		     			com.gree.shyun.bean+="	public void setId(long id){\n";
//		     			com.gree.shyun.bean+="		this.id = id ;\n";
//		     			com.gree.shyun.bean+="	}\n";
//		     			com.gree.shyun.bean+="	public long getId(){\n";
//		     			com.gree.shyun.bean+="		return id ;\n";
//		     			com.gree.shyun.bean+="	}\n";
//		     		}
                bean+=line;
                String addImportant="\n";
                if(hasList){
                    addImportant+=impList+"\n";
                }
                if(hasDate){
                    addImportant+=impDate+"\n";
                }
                if(cTable){
                    table.add(className);
                    addImportant+=impId+"\n";
//		     			if(hasId){
//		     				com.gree.shyun.bean = com.gree.shyun.bean.replaceAll("#ID#", "");
//		     			}else{
//		     				com.gree.shyun.bean = com.gree.shyun.bean.replaceAll("#ID#", "@Id\n	private long id;//主键ID");
//		     			}
                    cTable = false ;
                }else{
                    //com.gree.shyun.bean = com.gree.shyun.bean.replaceAll("#ID#", "");
                }
                bean = bean.replaceAll("#import#", addImportant);
                beans.put(className, bean);
                className="";
                temp="";
                bean="";
                hasList = false;
                hasId = false;
                hasDate = false ;
            }else{
                bean+=line;
            }
        }
        toBeanResult.setBeans(beans);
        toBeanResult.setTable(table);
        return toBeanResult;
    }
}
