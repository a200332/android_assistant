package com.gree.shyun.server.generatingcode;


import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

/**
 * 生成模板
 */
public class TemplateUtil {
    public static String create(String basePath,String vmName, Map<String,Object> datas){
        VelocityEngine ve = new VelocityEngine();
        Properties p = new Properties();
        p.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
        p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, basePath);
        ve.init(p);
        // 载入（获取）模板对象
        Template t = ve.getTemplate(vmName);
        VelocityContext ctx = new VelocityContext();
        for(String key:datas.keySet()){
            ctx.put(key,datas.get(key));
        }
        StringWriter sw = new StringWriter();
        t.merge(ctx, sw);
        return sw.toString();
    }

}
