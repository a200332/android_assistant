package com.gree.shyun.server.generatingcode;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.gree.shyun.server.db.util.LogUtils;
import com.gree.shyun.util.CommonUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonToModel {
    /**
     * 判断类型
     * @param val
     * @return
     */
    private static String getType(String val){
        String type = "String";
        try {
            Integer.parseInt(val);
            type="Integer";
        }catch (Exception e1){
            try{
                Float.parseFloat(val);
                type="Float";
            }catch(Exception e){
                try{
                    Double.parseDouble(val);
                    type="Double";
                }catch(Exception ee){
                    try{
                        Long.parseLong(val);
                        type="Long";
                    }catch(Exception eee){

                    }
                }
            }
        }
        return type;
    }
    /**
     * json转model
     * @param json
     * @return
     */
    public static Map<String,String> generate(String parent,String json){
        Map<String,String> jsonModels = new HashMap<String,String>();
        String model = "";
        //null处理 防止丢失null属性
        json = json.replaceAll(":null",":\"null\"");
        JSONObject job = JSON.parseObject(json, Feature.IgnoreNotMatch);
        Map<String,String> child = new HashMap<>();
        parent = parent.substring(0,1).toUpperCase().concat(parent.substring(1));

        model += parent+"{";
        model +="\n";
        for(String key:job.keySet()){
            Object obj = job.get(key);
            String type = "String";
            boolean isList = false ;
            boolean isObject = false ;
            boolean isArray = false ;
             if(obj instanceof String){
                String val = obj.toString();
                if(val.contains("格式为(")){
                    String tp = CommonUtil.getBetweenStr(val,"格式为\\(","\\)");
                    LogUtils.e(tp);
                    if(tp!=null){
                        if(tp.contains("int64")){
                            type = "Long";
                        }else if(tp.contains("int32")){
                            type = "Integer";
                        }else if(tp.contains("date")||tp.contains("time")){
                            //type = "Date";
                            type="String";//instant 类型 做处理
                        }else if(tp.contains("double")){
                            type = "Double";
                        }else if(tp.contains("float")){
                            type = "Float";
                        }else if(tp.contains("long")){
                            type = "Long";
                        }
                    }
                }else{
                    type = "String";
                }
            }else if(obj instanceof Integer){
                type = "Integer";
            }else if(obj instanceof List){
                String val = job.getString(key);
                if(val.length()>2){
                    String arrS = val.substring(1, val.length()-1);
                    if(arrS.startsWith("{")){
                        type = "List";
                        isList = true ;
                        //如果有多条数据，只取第一条数据
                        JSONArray arr = JSON.parseArray(val);
                        JSONObject arrChild  = arr.getJSONObject(0);
                        String arrStr = JSON.toJSON(arrChild).toString();
                        child.put(key,arrStr);
                    }else{
                        isArray = true;
                        if(arrS.length()>0){
                            if(arrS.contains(",")){
                                arrS = val.substring(0,arrS.indexOf(","));
                            }
                            type = getType(arrS);
                        }
                    }
                }else{
                    isArray = true;
                    if(val.length()>0){
                        if(val.contains(",")){
                            val = val.substring(0,val.indexOf(","));
                        }
                        type = getType(val);
                    }
                }

            }else if(obj instanceof Object){
                String val = job.getString(key);
                if(val!=null&&val.length()>=2){
                    if(val.startsWith("{")){
                        child.put(key, val);
                        type = "Object";
                        isObject = true ;
                        child.put(key, job.getString(key));
                    }
                }
                if(!isObject){
                    type = getType(val);
                }

            }

            if(isList){
                type =key.substring(0,1).toUpperCase().concat(key.substring(1));
                //model += "	private List<"+type+"> "+key+";";
                model+=key+"(List<"+type+">),";
                model +="\n";
                isList = false;
            }else if(isObject){
                type =key.substring(0,1).toUpperCase().concat(key.substring(1));
                //model += "	private "+type+" "+key+";";
                model += key+"("+type+"),";
                model +="\n";
                isObject = false;
            }else if(isArray){
                //model += "	private "+type+"[]"+" "+key+";";
                model +=key+"("+type+"[]),";
                model +="\n";
            }else{
                //model += "	private "+type+" "+key+";";
                model +=key+"("+type+"):"+obj.toString()+",";
                model +="\n";
            }
        }
        model += "}";
        model +="\n";
        jsonModels.put(parent, model);
        for(String key:child.keySet()){
            jsonModels.putAll(generate(key,child.get(key)));
        }
        return jsonModels;
    }
}
