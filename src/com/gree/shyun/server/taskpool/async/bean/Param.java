package com.gree.shyun.server.taskpool.async.bean;

import java.util.HashMap;
import java.util.Map;

public class Param {
    private Map<String,Object> param = new HashMap<>();
    public static final String defaultKey = "defaultKey";
    public Param add(Object value){
        param.put(defaultKey,value);
        return this;
    }
    public Param add(String key, Object value){
        param.put(key,value);
        return this;
    }
    private boolean hasParam(){
        return param!=null;
    }

    public Map<String,Object> getParam(){
        return param;
    }
    public <T>T get(){
        return get(defaultKey);
    }
    public <T>T get(String key){
        if(hasParam()){
            Object obj = param.get(key);
            if(obj!=null){
                try {
                    return (T)obj;
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
        return null;
    }
}
