package com.gree.shyun.server.taskpool.async.bean;

import java.util.HashMap;
import java.util.Map;

public class Result {
    private Map<String,Object> map = new HashMap<>();
    private static final String defaultKey = "defaultKey";
    public static Result empty(){
        return new Result();
    }
    public static Result put(Object value){
        Result result = new Result();
        result.put(defaultKey,value);
        return result;
    }
    public Result put(String key, Object value){
        map.put(key,value);
        return this;
    }
    private boolean hasParam(){
        return map!=null;
    }
    public <T>T get(){
        return get(defaultKey);
    }
    public <T>T get(String key){
        if(hasParam()&&map!=null){
            Object obj = map.get(key);
            if(obj!=null){
                try {
                    return (T)obj;
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
        return null;
    }
}
