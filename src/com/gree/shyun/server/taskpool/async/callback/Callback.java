package com.gree.shyun.server.taskpool.async.callback;

import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.worker.WorkResult;

public interface Callback {
   void result(boolean success, WorkResult<Result> result);
}
