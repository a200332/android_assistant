package com.gree.shyun.server.taskpool;

import com.gree.shyun.server.db.util.LogUtils;
import com.gree.shyun.server.taskpool.async.bean.Param;
import com.gree.shyun.server.taskpool.async.bean.Result;
import com.gree.shyun.server.taskpool.async.callback.Callback;
import com.gree.shyun.server.taskpool.async.callback.ICallback;
import com.gree.shyun.server.taskpool.async.callback.IWorker;
import com.gree.shyun.server.taskpool.async.executor.Async;
import com.gree.shyun.server.taskpool.async.worker.WorkResult;
import com.gree.shyun.server.taskpool.async.wrapper.WorkerWrapper;

import java.util.concurrent.ExecutionException;

public class TaskPool {
    private final static  int defaultTimeOut = 10*1000;//10秒
    private IWorker iWorker;
    private Param param = new Param();
    private Callback callback;
    public static TaskPool exec(Object obj){
        if(obj==null){
            return null;
        }
        IWorker iWorker = null;
        if(obj instanceof  IWorker){
            iWorker = (IWorker)obj;
        }else if(obj instanceof  Class){
            try {
                Class cls = (Class)obj;
                Object o = cls.newInstance();
                if(o instanceof IWorker){
                    iWorker=(IWorker)o;
                }else{
                    LogUtils.e("您传入执行的class不是IWorker类型!");
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }else if(obj instanceof String){
            try {
                Class cls = Class.forName(obj.toString());
                Object o = cls.newInstance();
                if(o instanceof IWorker){
                    iWorker=(IWorker)o;
                }else{
                    LogUtils.e("您传入执行的class不是IWorker类型!");
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(iWorker==null){
            return null;
        }
        return new TaskPool(iWorker);
    }
    public TaskPool(IWorker iWorker){
        if(iWorker==null){
            return;
        }
        this.iWorker = iWorker;
    }
    public TaskPool addParam(String key, Object value){
        param.add(key,value);
        return this;
    }
    public TaskPool addParam(Object value){
        param.add(value);
        return this;
    }
    public TaskPool callback(Callback callback){
        this.callback = callback;
        return  this;
    }
    public boolean start(){
        if(iWorker==null){
            LogUtils.e("异步线程执行代码块为空！");
            return false;
        }
        try {
            WorkerWrapper<Param, Result> workerWrapper = new WorkerWrapper.Builder<Param, Result>()
                    .worker(iWorker)
                    .param(param)
                    .callback((ICallback<Param, Result>) (success, param, workResult) -> {
                        if(callback!=null){
                            callback.result(success,workResult);
                        }
                    })
                    .build();
            Async.beginWork(defaultTimeOut, workerWrapper);
            return true;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

}
