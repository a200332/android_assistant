package com.gree.shyun.server.task;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * doTask 方法返回数据时一定是return this (回调)
 * or return null(不回调) ，不要返回其他的东西
 */
public abstract class Task implements Runnable, Serializable {

    //执行失败
    public static final int  ERROR = -1;
    public static final int SUCCESS = 0 ;
    public static final int CANCEL = 1 ;
    public static final int ALERT = 2;//弹提示窗
    /**
     * 这个会自动生成，不用自己设置
     */
    protected int uniqueID;
    /**
     * 主要是用来初始化的时候传入参数，
     * 然后根据不用的参数来进行异步操作
     */
    @SuppressWarnings("rawtypes")
    protected Map taskParam;// 内容参数
    /**
     * 异步操作完成之后的状态
     * 失败 -1 、成功 0  、 1 取消   其他 状态由自己在运行代码块里设置
     */
    protected int status;

    /**
     * 判断是否执行成功
     * @return
     */
    public boolean success(){
        return (status==SUCCESS);
    }
    /**
     *  执行失败的报错信息
     */
    protected String exception;
    /**
     * 如果是网络请求，并且获取的数据是Json,
     * 则直接可以给此字段赋值，然后在回调中get Json数据
     */
    protected String json;
    /**
     * 这个是异步操作后，如果想把异步数据传到UI线程，
     * 则可以通过此字段赋值，然后再强转得到所要的数据
     */
    protected Object result;


    public Task() {
    }

    public int getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }

    public boolean hasParam(){
        return taskParam!=null;
    }

    @SuppressWarnings("rawtypes")
    public Map getTaskParam() {
        return taskParam;
    }

    public <T>T getParam(String key){
        if(hasParam()){
            Object obj = taskParam.get(key);
            if(obj!=null){
                return (T)obj;
            }
        }
        return null;
    }
    public <T>T getRespone(){
        return getParam("respone");
    }
    public <T>T getRequest(){
        return getParam("request");
    }

    @SuppressWarnings("rawtypes")
    public Task setTaskParam(Map taskParam) {
        this.taskParam = taskParam;
        return this;
    }
    public Task set(String key,Object value){
        if(taskParam==null){
            taskParam = new HashMap();
        }
        taskParam.put(key,value);
        return this;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public <T>T getResult() {
            return (T)result;
    }

    public void setResult(Object result) {
        this.result = result;
    }



    @Override
    public void run() {
        doTask();
    }




    /**
     * 专门用来执行耗时的操作，
     * 子类只需要继承此类，实现此方法，
     * 在这个方法中执行所有耗时的操作
     * 用ExcuteTaskManager进行执行，
     * 可以回调， 也可以不回调
     *
     * 在继承此类的时候 doTask
     * return null（不再回调） or  return this(会回调)
     *
     * @return
     */
    public abstract Task doTask();

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    protected final void update(int flag, Object... args) {
        if (onListener != null) {
            onListener.onUpdate(flag, args);
        }
    }
    protected final void onCanceled(){
        if (onListener != null) {
            status = CANCEL;
            onListener.onCanceled();
        }
    }
    private OnListener onListener;

    public void setOnListener(OnListener onListener) {
        this.onListener = onListener;
    }

    public interface OnListener{
        void onUpdate(int flag, Object... args);
        //此方法需在doTask代码块中手动调用 自主取消程序运行
        void onCanceled();
    }
}
