package com.gree.shyun.server.db.util;

import java.util.Collection;
import java.util.Map;

public class TextUtils {
    public static  boolean isEmpty(String str){
        if(str!=null&&!"".equals(str)&&str.length()>0){
            return false;
        }
        return true;
    }
    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }
}
