package com.gree.shyun.server.db.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlUtil {
    /**
     * 将所有问号替换成值
     * @param sql
     * @param objs
     * @return
     */
    public static String replaceSql(String sql,Object[] objs){
        Pattern pattern = Pattern.compile("\\?",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(sql);
        StringBuffer result = new StringBuffer();
        int i = 0 ;
        while (matcher.find()) {
            if(i<objs.length) {
                Object obj = objs[i];
                boolean isStr = (obj instanceof String);
                matcher.appendReplacement(result, (isStr?"'":"")+String.valueOf(objs[i])+ (isStr?"'":""));
            }
            i++;
        }
        String last = sql.substring(sql.lastIndexOf("?")+1);
        sql = result.toString()+last;
        return sql;
    }
}
