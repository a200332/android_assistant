package com.gree.shyun.server.db.converter;


import com.gree.shyun.server.db.sqlite.ColumnDbType;
import com.gree.shyun.server.db.util.TextUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * Date: 13-11-4
 * Time: 下午10:51
 */
public class CharColumnConverter implements ColumnConverter<Character> {
    @Override
    public Character getFieldValue(final ResultSet resultSet, int index) throws SQLException {
        if(index<0){
            return null;
        }
        int value = resultSet.getInt(index);
        if(resultSet.wasNull()){
            return null;
        }
        return (char) value;
    }

    @Override
    public Character getFieldValue(String fieldStringValue) {
        if (TextUtils.isEmpty(fieldStringValue)) return null;
        return fieldStringValue.charAt(0);
    }

    @Override
    public Object fieldValue2ColumnValue(Character fieldValue) {
        if (fieldValue == null) return null;
        return (int) fieldValue;
    }

    @Override
    public ColumnDbType getColumnDbType() {
        return ColumnDbType.INTEGER;
    }
}
