package com.gree.shyun.server.db.converter;


import com.gree.shyun.server.db.sqlite.ColumnDbType;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * Date: 13-11-4
 * Time: 下午8:57
 */
public interface ColumnConverter<T> {

    T getFieldValue(final ResultSet resultSet, int index) throws SQLException;

    T getFieldValue(String fieldStringValue);

    Object fieldValue2ColumnValue(T fieldValue);

    ColumnDbType getColumnDbType();
}
