package com.gree.shyun.server.db.converter;


import com.gree.shyun.server.db.sqlite.ColumnDbType;
import com.gree.shyun.server.db.util.TextUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * Date: 13-11-4
 * Time: 下午10:51
 */
public class DateColumnConverter implements ColumnConverter<Date> {
    @Override
    public Date getFieldValue(final ResultSet resultSet, int index) throws SQLException {
        if(index<0){
            return null;
        }
        long value = resultSet.getLong(index);
        if(resultSet.wasNull()){
            return null;
        }
        return new Date(value);
    }

    @Override
    public Date getFieldValue(String fieldStringValue) {
        if (TextUtils.isEmpty(fieldStringValue)) return null;
        return new Date(Long.valueOf(fieldStringValue));
    }

    @Override
    public Object fieldValue2ColumnValue(Date fieldValue) {
        if (fieldValue == null) return null;
        return fieldValue.getTime();
    }

    @Override
    public ColumnDbType getColumnDbType() {
        return ColumnDbType.INTEGER;
    }
}
