package com.gree.shyun.server.db.converter;


import com.gree.shyun.server.db.sqlite.ColumnDbType;
import com.gree.shyun.server.db.util.TextUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * Date: 13-11-4
 * Time: 下午10:51
 */
public class IntegerColumnConverter implements ColumnConverter<Integer> {
    @Override
    public Integer getFieldValue(final ResultSet resultSet, int index) throws SQLException {
        if(index<0){
            return null;
        }
        int value = resultSet.getInt(index);
        if(resultSet.wasNull()){
            return null;
        }
        return value;
    }

    @Override
    public Integer getFieldValue(String fieldStringValue) {
        if (TextUtils.isEmpty(fieldStringValue)) return null;
        return Integer.valueOf(fieldStringValue);
    }

    @Override
    public Object fieldValue2ColumnValue(Integer fieldValue) {
        return fieldValue;
    }

    @Override
    public ColumnDbType getColumnDbType() {
        return ColumnDbType.INTEGER;
    }
}
