package com.gree.shyun.server;


import com.gree.shyun.server.http.util.HttpClient;
import com.gree.shyun.util.json.JsonMananger;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


public class BaseAction {

    public BaseAction(){
    }

    public static final String DOMAIN = "https://xueqiu.com";
    public static final String DOMAIN_QQ = "https://proxy.finance.qq.com";


    /**
     * post
     * @param domain
     * @param url
     * @param cla
     * @param obj
     * @param <T>
     * @return
     */
    public <T>T post(String domain,String url,Class<T> cla,Object obj) {
        String urls = new StringBuffer(domain).append("/").append(url).toString() ;
        HttpClient httpClientUtil = HttpClient.getInstance();
        httpClientUtil.setUrl(urls);
        httpClientUtil.setParameter(classToMap(obj));
        try {
            httpClientUtil.post();
            String json = httpClientUtil.getContent();
            return  JsonMananger.jsonToBean(json, cla);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     *  get
     * @param domain
     * @param url
     * @param cla
     * @param obj
     * @param <T>
     * @return
     */
    public <T>T get(String domain,String url,Class<T> cla,Object obj){
        String urls = new StringBuffer(domain).append("/").append(url).toString() ;
        HttpClient httpClientUtil = HttpClient.getInstance();
        httpClientUtil.setUrl(urls);
        httpClientUtil.setParameter(classToMap(obj));
        try {
            httpClientUtil.get();
            String json = httpClientUtil.getContent();
            System.out.println(json);
            return  JsonMananger.jsonToBean(json, cla);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 类转map
     * @param obj
     * @return
     */
    public static Map<String,Object> classToMap(Object obj){
        Map<String,Object> map = new HashMap<>();
        if(obj==null){
            return map;
        }
        Class<?> cla = obj.getClass();
        Field[] fa =  cla.getDeclaredFields();;
        int i = 0 ;
        for(Field f:fa){
            f.setAccessible(true);
            try {
                Object val = f.get(obj);
                if(val!=null) {
                    map.put(f.getName(),val);
                }
            }catch (Exception e){
            }
        }
        return map;
    }


    /**
     * 获取完整URL属性值
     * @param url
     * @param obj
     * @return
     */
    protected static String getURL(String url, Object obj) {
        StringBuilder urlBuilder = new StringBuilder(url);
        if(obj!=null){
            urlBuilder.append("?");
            Class<?> cla = obj.getClass();
            Field[] fa =  cla.getDeclaredFields();;
            int i = 0 ;
            for(Field f:fa){
                f.setAccessible(true);

                try {
                    Object val = f.get(obj);
                    if(val!=null) {
                        String name = f.getName();
                        urlBuilder.append(name + "=" + val);
                        if (i < fa.length - 1) {
                            urlBuilder.append("&");
                        }
                    }
                }catch (Exception e){
                }
                i++;
            }
        }
        return urlBuilder.toString();
    }


}
