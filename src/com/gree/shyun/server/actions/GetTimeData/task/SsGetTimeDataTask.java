package com.gree.shyun.server.actions.GetTimeData.task;

import com.gree.shyun.server.actions.GetTimeData.SsGetTimeDataAction;
import com.gree.shyun.server.actions.GetTimeData.requestParam.SsGetTimeDataParam;
import com.gree.shyun.server.actions.GetTimeData.respone.SsGetTimeDataRespone;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.server.task.TaskManager;
import com.gree.shyun.util.json.JsonMananger;

import java.util.Random;

/**
* ${remark} 任务
* Created by youdelu on 2022-04-28 09:10:07.
*/
public class SsGetTimeDataTask extends Task {
    final static String TAG = SsGetTimeDataTask.class.getSimpleName();
    @Override
    public Task doTask() {
		SsGetTimeDataParam ssGetTimeDataRequestParam = new SsGetTimeDataParam();
		ssGetTimeDataRequestParam.setRandom(new Random().nextFloat()+"");//0.11887449850378018,
		ssGetTimeDataRequestParam.setCode("001213");//000008,
		ssGetTimeDataRequestParam.setMarketId("1");//1,
        SsGetTimeDataAction action= new SsGetTimeDataAction();

        SsGetTimeDataRespone respone = action.get(ssGetTimeDataRequestParam);
        System.out.println(JsonMananger.beanToJsonStr(respone));
        return this;
    }
    public static void main(String[] args){
        SsGetTimeDataTask task = new SsGetTimeDataTask();
        TaskManager.getInstance().exec(task, t -> {

        });
    }
}