package com.gree.shyun.server.actions.GetTimeData.requestParam;


/**
* Created by youdelu on 2022-04-28 09:10:07.
*
*/
public class SsGetTimeDataParam {
	private String random;//0.11887449850378018
	private String code;//000008
	private String marketId;//1

	public void setRandom(String random){
		this.random=random;
	}
	public String getRandom(){
		return random;
	}

	public void setCode(String code){
		this.code=code;
	}
	public String getCode(){
		return code;
	}

	public void setMarketId(String marketId){
		this.marketId=marketId;
	}
	public String getMarketId(){
		return marketId;
	}
}