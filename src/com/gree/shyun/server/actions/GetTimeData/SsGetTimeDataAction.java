package com.gree.shyun.server.actions.GetTimeData;


import com.gree.shyun.server.BaseAction;
import com.gree.shyun.server.actions.GetTimeData.requestParam.SsGetTimeDataParam;
import com.gree.shyun.server.actions.GetTimeData.respone.SsGetTimeDataRespone;


/**
 * 获取当日分时
 * Created by youdelu on 2022-04-28 09:10:07.
 */
public class SsGetTimeDataAction extends BaseAction {

    public <T>T get(SsGetTimeDataParam param)   {
        return (T)get(DOMAIN,getURL("market/ssjjhq/getTimeData",param), SsGetTimeDataRespone.class , null );
    }
}