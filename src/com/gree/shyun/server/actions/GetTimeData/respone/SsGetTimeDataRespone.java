package com.gree.shyun.server.actions.GetTimeData.respone;


/**
* Created by youdelu on 2022-04-28 09:10:07.
*
*/
public class SsGetTimeDataRespone{
	private String datetime;//2021-12-29 10:12
	private String code;//0
	private Data data;
	private String message;//成功

	public void setDatetime(String datetime){
		this.datetime=datetime;
	}
	public String getDatetime(){
		return datetime;
	}

	public void setCode(String code){
		this.code=code;
	}
	public String getCode(){
		return code;
	}

	public void setData(Data data){
		this.data=data;
	}
	public Data getData(){
		return data;
	}

	public void setMessage(String message){
		this.message=message;
	}
	public String getMessage(){
		return message;
	}
}