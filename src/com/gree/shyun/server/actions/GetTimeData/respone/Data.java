package com.gree.shyun.server.actions.GetTimeData.respone;

import java.util.List;

/**
* Created by youdelu on 2022-04-28 09:10:07.
*
*/
public class Data{
	private String code;//000008
	private Integer groupId;//1
	private String delta;//-0.06
	private String high;//2.71
	private String low;//2.60
	private List<Sellbuy1> sellbuy1;
	private String now;//2.66
	private Float amountAhT;//0.0
	private String[] picupdata;
	private String isCDR;//false
	private String close;//2.72
	private String deltaPercent;//-2.21
	private Integer isRegistration;//0
	private Float amount;//150778177.95
	private Integer lastVolume;//136
	private Integer isNoProfit;//0
	private List<Sellbuy5> sellbuy5;
	private String tradingPhaseCode1;//3
	private String[] picdowndata;
	private String tradingPhaseCode2;//null
	private Integer volume;//571529
	private String isDelisting;//null
	private String change20PerLimit;//false
	private Integer isVIE;//0
	private Integer volumeAhT;//0
	private String name;//神州高铁
	private Integer isVoteDifferent;//0
	private String[] picavgprice;
	private String open;//2.69
	private String marketTime;//2021-12-29 10:12:14

	public void setCode(String code){
		this.code=code;
	}
	public String getCode(){
		return code;
	}

	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	public Integer getGroupId(){
		return groupId;
	}

	public void setDelta(String delta){
		this.delta=delta;
	}
	public String getDelta(){
		return delta;
	}

	public void setHigh(String high){
		this.high=high;
	}
	public String getHigh(){
		return high;
	}

	public void setLow(String low){
		this.low=low;
	}
	public String getLow(){
		return low;
	}

	public void setSellbuy1(List<Sellbuy1> sellbuy1){
		this.sellbuy1=sellbuy1;
	}
	public List<Sellbuy1> getSellbuy1(){
		return sellbuy1;
	}

	public void setNow(String now){
		this.now=now;
	}
	public String getNow(){
		return now;
	}

	public void setAmountAhT(Float amountAhT){
		this.amountAhT=amountAhT;
	}
	public Float getAmountAhT(){
		return amountAhT;
	}

	public void setPicupdata(String[] picupdata){
		this.picupdata=picupdata;
	}
	public String[] getPicupdata(){
		return picupdata;
	}

	public void setIsCDR(String isCDR){
		this.isCDR=isCDR;
	}
	public String getIsCDR(){
		return isCDR;
	}

	public void setClose(String close){
		this.close=close;
	}
	public String getClose(){
		return close;
	}

	public void setDeltaPercent(String deltaPercent){
		this.deltaPercent=deltaPercent;
	}
	public String getDeltaPercent(){
		return deltaPercent;
	}

	public void setIsRegistration(Integer isRegistration){
		this.isRegistration=isRegistration;
	}
	public Integer getIsRegistration(){
		return isRegistration;
	}

	public void setAmount(Float amount){
		this.amount=amount;
	}
	public Float getAmount(){
		return amount;
	}

	public void setLastVolume(Integer lastVolume){
		this.lastVolume=lastVolume;
	}
	public Integer getLastVolume(){
		return lastVolume;
	}

	public void setIsNoProfit(Integer isNoProfit){
		this.isNoProfit=isNoProfit;
	}
	public Integer getIsNoProfit(){
		return isNoProfit;
	}

	public void setSellbuy5(List<Sellbuy5> sellbuy5){
		this.sellbuy5=sellbuy5;
	}
	public List<Sellbuy5> getSellbuy5(){
		return sellbuy5;
	}

	public void setTradingPhaseCode1(String tradingPhaseCode1){
		this.tradingPhaseCode1=tradingPhaseCode1;
	}
	public String getTradingPhaseCode1(){
		return tradingPhaseCode1;
	}

	public void setPicdowndata(String[] picdowndata){
		this.picdowndata=picdowndata;
	}
	public String[] getPicdowndata(){
		return picdowndata;
	}

	public void setTradingPhaseCode2(String tradingPhaseCode2){
		this.tradingPhaseCode2=tradingPhaseCode2;
	}
	public String getTradingPhaseCode2(){
		return tradingPhaseCode2;
	}

	public void setVolume(Integer volume){
		this.volume=volume;
	}
	public Integer getVolume(){
		return volume;
	}

	public void setIsDelisting(String isDelisting){
		this.isDelisting=isDelisting;
	}
	public String getIsDelisting(){
		return isDelisting;
	}

	public void setChange20PerLimit(String change20PerLimit){
		this.change20PerLimit=change20PerLimit;
	}
	public String getChange20PerLimit(){
		return change20PerLimit;
	}

	public void setIsVIE(Integer isVIE){
		this.isVIE=isVIE;
	}
	public Integer getIsVIE(){
		return isVIE;
	}

	public void setVolumeAhT(Integer volumeAhT){
		this.volumeAhT=volumeAhT;
	}
	public Integer getVolumeAhT(){
		return volumeAhT;
	}

	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}

	public void setIsVoteDifferent(Integer isVoteDifferent){
		this.isVoteDifferent=isVoteDifferent;
	}
	public Integer getIsVoteDifferent(){
		return isVoteDifferent;
	}

	public void setPicavgprice(String[] picavgprice){
		this.picavgprice=picavgprice;
	}
	public String[] getPicavgprice(){
		return picavgprice;
	}

	public void setOpen(String open){
		this.open=open;
	}
	public String getOpen(){
		return open;
	}

	public void setMarketTime(String marketTime){
		this.marketTime=marketTime;
	}
	public String getMarketTime(){
		return marketTime;
	}
}