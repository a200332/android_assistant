package com.gree.shyun.server.actions.InfoSearch.requestParam;


/**
* Created by youdelu on 2022-03-08 14:12:52.
*
*/
public class InSearchParam{
	private String symbol_id;//sh600077
	private int count;//1.0
	private String source;//2
	private int page;//0

	public String getSymbol_id() {
		return symbol_id;
	}

	public void setSymbol_id(String symbol_id) {
		this.symbol_id = symbol_id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}