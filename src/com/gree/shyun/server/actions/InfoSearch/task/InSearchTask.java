package com.gree.shyun.server.actions.InfoSearch.task;

import com.gree.shyun.server.BaseAction;
import com.gree.shyun.server.actions.InfoSearch.InSearchAction;
import com.gree.shyun.server.actions.InfoSearch.requestParam.InSearchParam;
import com.gree.shyun.server.actions.InfoSearch.respone.InSearchRespone;
import com.gree.shyun.server.actions.InfoSearch.respone.MessageInfo;
import com.gree.shyun.server.actions.QuoteList.respone.GpData;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.http.util.HttpClient;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.util.CommonUtil;
import com.gree.shyun.util.json.JsonMananger;

import java.io.IOException;
import java.util.List;

/**
* ${remark} 任务
* Created by youdelu on 2022-03-08 14:12:52.
*/
public class InSearchTask extends Task {
    final static String TAG = InSearchTask.class.getSimpleName();
    @Override
    public Task doTask() {
        gpDataList = DbHelper.findAll(GpData.class);

        //先请求一下首页 绕开防爬策略
        HttpClient httpClient = HttpClient.getInstance();
        httpClient.setUrl(BaseAction.DOMAIN);
        try {
            httpClient.get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        toGetMessage();
        return this;
    }

    private List<GpData> gpDataList;
    private GpData tempGpData;
    private int gpIndex = 0;
    private int messagePageIndex = 0;
    private void toGetMessage(){
        if(gpDataList==null){
            return;
        }
        tempGpData = gpDataList.get(gpIndex);

        messagePageIndex++;

        try {
            InSearchParam inSearchParam = new InSearchParam();
            inSearchParam.setSymbol_id(tempGpData.getSymbol());//sh600077,
            inSearchParam.setPage(messagePageIndex);//2,
            inSearchParam.setCount(50);
            inSearchParam.setSource("公告");
            InSearchAction action = new InSearchAction();
            System.out.println(JsonMananger.beanToJsonStr(inSearchParam));
            InSearchRespone respone = action.get(inSearchParam);
            if (respone.getList()!= null && messagePageIndex < respone.getMaxPage()) {
                List<MessageInfo> gpMessages = respone.getList();
                boolean shouldNext = false;
                if (gpMessages != null && gpMessages.size() > 0) {
                    for (MessageInfo gpMessage : gpMessages) {
                        MessageInfo gpMes = DbHelper.findById(MessageInfo.class, gpMessage.getId());
                        if (gpMes != null) {
                            System.out.println("存在:" + gpMes.getTitle());
                            shouldNext = true;
                        }
                        gpMessage.setTitle(CommonUtil.unicodeToUtf8(gpMessage.getTitle()));
                        System.out.println(gpMessage.getTitle());
                    }
                    DbHelper.saveOrUpdate(gpMessages);
                } else {
                    shouldNext = true;
                }
                if (shouldNext) {
                    gpIndex++;
                    messagePageIndex = 0;
                }
            } else {
                gpIndex++;
                messagePageIndex = 0;
            }
        }catch (Exception e){

        }

        if(gpIndex<gpDataList.size()) {
            try {
                System.gc();
                Thread.sleep(3000);
            }catch (Exception e){

            }
            toGetMessage();
        }
    }
}