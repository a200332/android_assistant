package com.gree.shyun.server.actions.InfoSearch.task;

import com.gree.shyun.server.actions.InfoSearch.respone.InSearchRespone;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.server.task.TaskManager;
import com.gree.shyun.util.json.JsonMananger;

/**
 * 使用方法（测试用）
 * Created by youdelu on 2020-12-14 08:57:28.
 */
public class UseInSearchTask {
    public static final String TAG = UseInSearchTask.class.getSimpleName();
    public static void runTask(){
        InSearchTask inSearchTask = new InSearchTask();
        inSearchTask.set("data",TAG);//传递任意参数
        TaskManager.getInstance().exec(inSearchTask, task->{
            if(task.getStatus()== Task.SUCCESS){
                InSearchRespone respone = task.getParam("respone");
                System.out.println(JsonMananger.beanToJsonStr(respone));
            }else{
                System.out.println("出错了:"+task.getException());
            }
        });
    }
    public static void main(String[] args){
        UseInSearchTask.runTask();
    }
}
