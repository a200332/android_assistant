package com.gree.shyun.server.actions.InfoSearch;


import com.gree.shyun.server.BaseAction;

import com.gree.shyun.server.actions.InfoSearch.requestParam.InSearchParam;
import com.gree.shyun.server.actions.InfoSearch.respone.InSearchRespone;

/**
 * ${remark}
 * Created by youdelu on 2022-03-08 14:12:52.
 */
public class InSearchAction extends BaseAction {
    public InSearchAction() {
        super();
    }
    public <T>T get(InSearchParam param) {
        return (T)get(DOMAIN,getURL("statuses/stock_timeline.json",param), InSearchRespone.class , null );
    }
}