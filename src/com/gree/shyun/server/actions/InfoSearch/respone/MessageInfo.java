/**
  * Copyright 2022 json.cn 
  */
package com.gree.shyun.server.actions.InfoSearch.respone;
import com.gree.shyun.server.actions.QuoteList.respone.GpData;
import com.gree.shyun.server.db.annotation.Id;
import com.gree.shyun.server.db.annotation.NoAutoIncrement;
import com.gree.shyun.server.db.annotation.Table;

import java.util.List;

/**
 * Auto-generated: 2022-03-14 22:49:43
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
@Table
public class MessageInfo {

    @Id
    @NoAutoIncrement
    private String id;
    private int user_id;
    private String source;
    private String title;
    private long created_at;
    private int retweet_count;
    private int reply_count;
    private int fav_count;
    private boolean truncated;
    private int commentId;
    private int retweet_status_id;
    private String symbol_id;
    private String description;
    private String type;
    private String source_link;
    private String edited_at;
    private User user;
    private String retweeted_status;
    private String answers;
    private String rqtype;
    private int rqid;
    private String target;
    private String fragment;
    private boolean blocked;
    private boolean blocking;
    private String topic_pic;
    private String topic_symbol;
    private String topic_title;
    private String topic_desc;
    private int donate_count;
    private int donate_snowcoin;
    private boolean liked;
    private int view_count;
    private int weixin_retweet_count;
    private int mark;
    private String card;
    private boolean favorited;
    private String favorited_created_at;
    private String offer;
    private String score;
    private boolean editable;
    private String fundxHold;
    private List<String> stockCorrelation;
    private boolean reward;
    private boolean longTextForIOS;
    private String rawTitle;
    private String tagStr;
    private boolean canEdit;
    private String firstImg;
    private String timeBefore;
    private boolean expend;
    private String topic_pic_thumbnail_small;
    private String topic_pic_thumbnail;
    private String topic_pic_headOrPad;
    private String tagsForWeb;
    private String meta_keywords;
    private String paid_mention;
    private int reward_count;
    private int reward_amount;
    private int reward_user_count;
    private int talk_count;
    private int like_count;
    private String video_info;
    private List<Quote_cards> quote_cards;
    private String promotion_pic;
    private String promotion_url;
    private int promotion_id;
    private String mark_desc;
    private List<String> pic_sizes;
    private String cover_pic_size;
    private int order_id;
    private List<String> tags;
    private String status_industry;
    private List<String> excellent_comments;
    private String notice_tag;
    private String common_emotion;
    private String current_stock_price;
    private String new_card;
    private String source_deep_link;
    private boolean source_feed;
    private String like_config;
    private String title_ad_pic;
    private String title_ad_url;
    private String title_ad_deep_link;
    private String answer_comment;
    private int answer_count;
    private String answer_users;
    private String bonus_screen_name;
    private String fundx_tag;
    private String stock_list;
    private String reply_user_images;
    private int reply_user_count;
    private String fundx_symbol;
    private String hot_new_rank;
    private boolean is_answer;
    private boolean is_refused;
    private String text;
    private boolean forbidden_retweet;
    private boolean legal_user_visible;
    private boolean show_cover_pic;
    private boolean is_column;
    private String cover_pic;
    private boolean is_ss_multi_pic;
    private boolean is_bonus;
    private String vod_info;
    private boolean controversial;
    private String pic;
    private String recommend_cards;
    private boolean is_no_archive;
    private boolean is_original_declare;
    private boolean mp_not_show_status;
    private boolean forbidden_comment;
    private boolean answer_question;
    private boolean is_private;

    private GpData gpData;

    public GpData getGpData() {
        return gpData;
    }

    public void setGpData(GpData gpData) {
        this.gpData = gpData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public int getRetweet_count() {
        return retweet_count;
    }

    public void setRetweet_count(int retweet_count) {
        this.retweet_count = retweet_count;
    }

    public int getReply_count() {
        return reply_count;
    }

    public void setReply_count(int reply_count) {
        this.reply_count = reply_count;
    }

    public int getFav_count() {
        return fav_count;
    }

    public void setFav_count(int fav_count) {
        this.fav_count = fav_count;
    }

    public boolean isTruncated() {
        return truncated;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getRetweet_status_id() {
        return retweet_status_id;
    }

    public void setRetweet_status_id(int retweet_status_id) {
        this.retweet_status_id = retweet_status_id;
    }

    public String getSymbol_id() {
        return symbol_id;
    }

    public void setSymbol_id(String symbol_id) {
        this.symbol_id = symbol_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource_link() {
        return source_link;
    }

    public void setSource_link(String source_link) {
        this.source_link = source_link;
    }

    public String getEdited_at() {
        return edited_at;
    }

    public void setEdited_at(String edited_at) {
        this.edited_at = edited_at;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRetweeted_status() {
        return retweeted_status;
    }

    public void setRetweeted_status(String retweeted_status) {
        this.retweeted_status = retweeted_status;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getRqtype() {
        return rqtype;
    }

    public void setRqtype(String rqtype) {
        this.rqtype = rqtype;
    }

    public int getRqid() {
        return rqid;
    }

    public void setRqid(int rqid) {
        this.rqid = rqid;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getFragment() {
        return fragment;
    }

    public void setFragment(String fragment) {
        this.fragment = fragment;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isBlocking() {
        return blocking;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }

    public String getTopic_pic() {
        return topic_pic;
    }

    public void setTopic_pic(String topic_pic) {
        this.topic_pic = topic_pic;
    }

    public String getTopic_symbol() {
        return topic_symbol;
    }

    public void setTopic_symbol(String topic_symbol) {
        this.topic_symbol = topic_symbol;
    }

    public String getTopic_title() {
        return topic_title;
    }

    public void setTopic_title(String topic_title) {
        this.topic_title = topic_title;
    }

    public String getTopic_desc() {
        return topic_desc;
    }

    public void setTopic_desc(String topic_desc) {
        this.topic_desc = topic_desc;
    }

    public int getDonate_count() {
        return donate_count;
    }

    public void setDonate_count(int donate_count) {
        this.donate_count = donate_count;
    }

    public int getDonate_snowcoin() {
        return donate_snowcoin;
    }

    public void setDonate_snowcoin(int donate_snowcoin) {
        this.donate_snowcoin = donate_snowcoin;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public int getWeixin_retweet_count() {
        return weixin_retweet_count;
    }

    public void setWeixin_retweet_count(int weixin_retweet_count) {
        this.weixin_retweet_count = weixin_retweet_count;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public boolean isFavorited() {
        return favorited;
    }

    public void setFavorited(boolean favorited) {
        this.favorited = favorited;
    }

    public String getFavorited_created_at() {
        return favorited_created_at;
    }

    public void setFavorited_created_at(String favorited_created_at) {
        this.favorited_created_at = favorited_created_at;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getFundxHold() {
        return fundxHold;
    }

    public void setFundxHold(String fundxHold) {
        this.fundxHold = fundxHold;
    }

    public List<String> getStockCorrelation() {
        return stockCorrelation;
    }

    public void setStockCorrelation(List<String> stockCorrelation) {
        this.stockCorrelation = stockCorrelation;
    }

    public boolean isReward() {
        return reward;
    }

    public void setReward(boolean reward) {
        this.reward = reward;
    }

    public boolean isLongTextForIOS() {
        return longTextForIOS;
    }

    public void setLongTextForIOS(boolean longTextForIOS) {
        this.longTextForIOS = longTextForIOS;
    }

    public String getRawTitle() {
        return rawTitle;
    }

    public void setRawTitle(String rawTitle) {
        this.rawTitle = rawTitle;
    }

    public String getTagStr() {
        return tagStr;
    }

    public void setTagStr(String tagStr) {
        this.tagStr = tagStr;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public String getFirstImg() {
        return firstImg;
    }

    public void setFirstImg(String firstImg) {
        this.firstImg = firstImg;
    }

    public String getTimeBefore() {
        return timeBefore;
    }

    public void setTimeBefore(String timeBefore) {
        this.timeBefore = timeBefore;
    }

    public boolean isExpend() {
        return expend;
    }

    public void setExpend(boolean expend) {
        this.expend = expend;
    }

    public String getTopic_pic_thumbnail_small() {
        return topic_pic_thumbnail_small;
    }

    public void setTopic_pic_thumbnail_small(String topic_pic_thumbnail_small) {
        this.topic_pic_thumbnail_small = topic_pic_thumbnail_small;
    }

    public String getTopic_pic_thumbnail() {
        return topic_pic_thumbnail;
    }

    public void setTopic_pic_thumbnail(String topic_pic_thumbnail) {
        this.topic_pic_thumbnail = topic_pic_thumbnail;
    }

    public String getTopic_pic_headOrPad() {
        return topic_pic_headOrPad;
    }

    public void setTopic_pic_headOrPad(String topic_pic_headOrPad) {
        this.topic_pic_headOrPad = topic_pic_headOrPad;
    }

    public String getTagsForWeb() {
        return tagsForWeb;
    }

    public void setTagsForWeb(String tagsForWeb) {
        this.tagsForWeb = tagsForWeb;
    }

    public String getMeta_keywords() {
        return meta_keywords;
    }

    public void setMeta_keywords(String meta_keywords) {
        this.meta_keywords = meta_keywords;
    }

    public String getPaid_mention() {
        return paid_mention;
    }

    public void setPaid_mention(String paid_mention) {
        this.paid_mention = paid_mention;
    }

    public int getReward_count() {
        return reward_count;
    }

    public void setReward_count(int reward_count) {
        this.reward_count = reward_count;
    }

    public int getReward_amount() {
        return reward_amount;
    }

    public void setReward_amount(int reward_amount) {
        this.reward_amount = reward_amount;
    }

    public int getReward_user_count() {
        return reward_user_count;
    }

    public void setReward_user_count(int reward_user_count) {
        this.reward_user_count = reward_user_count;
    }

    public int getTalk_count() {
        return talk_count;
    }

    public void setTalk_count(int talk_count) {
        this.talk_count = talk_count;
    }

    public int getLike_count() {
        return like_count;
    }

    public void setLike_count(int like_count) {
        this.like_count = like_count;
    }

    public String getVideo_info() {
        return video_info;
    }

    public void setVideo_info(String video_info) {
        this.video_info = video_info;
    }

    public List<Quote_cards> getQuote_cards() {
        return quote_cards;
    }

    public void setQuote_cards(List<Quote_cards> quote_cards) {
        this.quote_cards = quote_cards;
    }

    public String getPromotion_pic() {
        return promotion_pic;
    }

    public void setPromotion_pic(String promotion_pic) {
        this.promotion_pic = promotion_pic;
    }

    public String getPromotion_url() {
        return promotion_url;
    }

    public void setPromotion_url(String promotion_url) {
        this.promotion_url = promotion_url;
    }

    public int getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(int promotion_id) {
        this.promotion_id = promotion_id;
    }

    public String getMark_desc() {
        return mark_desc;
    }

    public void setMark_desc(String mark_desc) {
        this.mark_desc = mark_desc;
    }

    public List<String> getPic_sizes() {
        return pic_sizes;
    }

    public void setPic_sizes(List<String> pic_sizes) {
        this.pic_sizes = pic_sizes;
    }

    public String getCover_pic_size() {
        return cover_pic_size;
    }

    public void setCover_pic_size(String cover_pic_size) {
        this.cover_pic_size = cover_pic_size;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getStatus_industry() {
        return status_industry;
    }

    public void setStatus_industry(String status_industry) {
        this.status_industry = status_industry;
    }

    public List<String> getExcellent_comments() {
        return excellent_comments;
    }

    public void setExcellent_comments(List<String> excellent_comments) {
        this.excellent_comments = excellent_comments;
    }

    public String getNotice_tag() {
        return notice_tag;
    }

    public void setNotice_tag(String notice_tag) {
        this.notice_tag = notice_tag;
    }

    public String getCommon_emotion() {
        return common_emotion;
    }

    public void setCommon_emotion(String common_emotion) {
        this.common_emotion = common_emotion;
    }

    public String getCurrent_stock_price() {
        return current_stock_price;
    }

    public void setCurrent_stock_price(String current_stock_price) {
        this.current_stock_price = current_stock_price;
    }

    public String getNew_card() {
        return new_card;
    }

    public void setNew_card(String new_card) {
        this.new_card = new_card;
    }

    public String getSource_deep_link() {
        return source_deep_link;
    }

    public void setSource_deep_link(String source_deep_link) {
        this.source_deep_link = source_deep_link;
    }

    public boolean isSource_feed() {
        return source_feed;
    }

    public void setSource_feed(boolean source_feed) {
        this.source_feed = source_feed;
    }

    public String getLike_config() {
        return like_config;
    }

    public void setLike_config(String like_config) {
        this.like_config = like_config;
    }

    public String getTitle_ad_pic() {
        return title_ad_pic;
    }

    public void setTitle_ad_pic(String title_ad_pic) {
        this.title_ad_pic = title_ad_pic;
    }

    public String getTitle_ad_url() {
        return title_ad_url;
    }

    public void setTitle_ad_url(String title_ad_url) {
        this.title_ad_url = title_ad_url;
    }

    public String getTitle_ad_deep_link() {
        return title_ad_deep_link;
    }

    public void setTitle_ad_deep_link(String title_ad_deep_link) {
        this.title_ad_deep_link = title_ad_deep_link;
    }

    public String getAnswer_comment() {
        return answer_comment;
    }

    public void setAnswer_comment(String answer_comment) {
        this.answer_comment = answer_comment;
    }

    public int getAnswer_count() {
        return answer_count;
    }

    public void setAnswer_count(int answer_count) {
        this.answer_count = answer_count;
    }

    public String getAnswer_users() {
        return answer_users;
    }

    public void setAnswer_users(String answer_users) {
        this.answer_users = answer_users;
    }

    public String getBonus_screen_name() {
        return bonus_screen_name;
    }

    public void setBonus_screen_name(String bonus_screen_name) {
        this.bonus_screen_name = bonus_screen_name;
    }

    public String getFundx_tag() {
        return fundx_tag;
    }

    public void setFundx_tag(String fundx_tag) {
        this.fundx_tag = fundx_tag;
    }

    public String getStock_list() {
        return stock_list;
    }

    public void setStock_list(String stock_list) {
        this.stock_list = stock_list;
    }

    public String getReply_user_images() {
        return reply_user_images;
    }

    public void setReply_user_images(String reply_user_images) {
        this.reply_user_images = reply_user_images;
    }

    public int getReply_user_count() {
        return reply_user_count;
    }

    public void setReply_user_count(int reply_user_count) {
        this.reply_user_count = reply_user_count;
    }

    public String getFundx_symbol() {
        return fundx_symbol;
    }

    public void setFundx_symbol(String fundx_symbol) {
        this.fundx_symbol = fundx_symbol;
    }

    public String getHot_new_rank() {
        return hot_new_rank;
    }

    public void setHot_new_rank(String hot_new_rank) {
        this.hot_new_rank = hot_new_rank;
    }

    public boolean isIs_answer() {
        return is_answer;
    }

    public void setIs_answer(boolean is_answer) {
        this.is_answer = is_answer;
    }

    public boolean isIs_refused() {
        return is_refused;
    }

    public void setIs_refused(boolean is_refused) {
        this.is_refused = is_refused;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isForbidden_retweet() {
        return forbidden_retweet;
    }

    public void setForbidden_retweet(boolean forbidden_retweet) {
        this.forbidden_retweet = forbidden_retweet;
    }

    public boolean isLegal_user_visible() {
        return legal_user_visible;
    }

    public void setLegal_user_visible(boolean legal_user_visible) {
        this.legal_user_visible = legal_user_visible;
    }

    public boolean isShow_cover_pic() {
        return show_cover_pic;
    }

    public void setShow_cover_pic(boolean show_cover_pic) {
        this.show_cover_pic = show_cover_pic;
    }

    public boolean isIs_column() {
        return is_column;
    }

    public void setIs_column(boolean is_column) {
        this.is_column = is_column;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public boolean isIs_ss_multi_pic() {
        return is_ss_multi_pic;
    }

    public void setIs_ss_multi_pic(boolean is_ss_multi_pic) {
        this.is_ss_multi_pic = is_ss_multi_pic;
    }

    public boolean isIs_bonus() {
        return is_bonus;
    }

    public void setIs_bonus(boolean is_bonus) {
        this.is_bonus = is_bonus;
    }

    public String getVod_info() {
        return vod_info;
    }

    public void setVod_info(String vod_info) {
        this.vod_info = vod_info;
    }

    public boolean isControversial() {
        return controversial;
    }

    public void setControversial(boolean controversial) {
        this.controversial = controversial;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getRecommend_cards() {
        return recommend_cards;
    }

    public void setRecommend_cards(String recommend_cards) {
        this.recommend_cards = recommend_cards;
    }

    public boolean isIs_no_archive() {
        return is_no_archive;
    }

    public void setIs_no_archive(boolean is_no_archive) {
        this.is_no_archive = is_no_archive;
    }

    public boolean isIs_original_declare() {
        return is_original_declare;
    }

    public void setIs_original_declare(boolean is_original_declare) {
        this.is_original_declare = is_original_declare;
    }

    public boolean isMp_not_show_status() {
        return mp_not_show_status;
    }

    public void setMp_not_show_status(boolean mp_not_show_status) {
        this.mp_not_show_status = mp_not_show_status;
    }

    public boolean isForbidden_comment() {
        return forbidden_comment;
    }

    public void setForbidden_comment(boolean forbidden_comment) {
        this.forbidden_comment = forbidden_comment;
    }

    public boolean isAnswer_question() {
        return answer_question;
    }

    public void setAnswer_question(boolean answer_question) {
        this.answer_question = answer_question;
    }

    public boolean isIs_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }
}