/**
  * Copyright 2022 json.cn 
  */
package com.gree.shyun.server.actions.InfoSearch.respone;

/**
 * Auto-generated: 2022-03-14 22:49:43
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Quote_cards {

    private String title;
    private String host;
    private String img_url;
    private String img_type;
    private String target_url;
    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setHost(String host) {
         this.host = host;
     }
     public String getHost() {
         return host;
     }

    public void setImg_url(String img_url) {
         this.img_url = img_url;
     }
     public String getImg_url() {
         return img_url;
     }

    public void setImg_type(String img_type) {
         this.img_type = img_type;
     }
     public String getImg_type() {
         return img_type;
     }

    public void setTarget_url(String target_url) {
         this.target_url = target_url;
     }
     public String getTarget_url() {
         return target_url;
     }

}