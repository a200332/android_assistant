/**
  * Copyright 2022 json.cn 
  */
package com.gree.shyun.server.actions.InfoSearch.respone;

/**
 * Auto-generated: 2022-03-14 22:49:43
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class User {

    private boolean subscribeable;
    private int common_count;
    private String remark;
    private String recommend_reason;
    private String stock_status_count;
    private String province;
    private String city;
    private String gender;
    private String blog_description;
    private String stocks_count;
    private String step;
    private String recommend;
    private int last_status_id;
    private String status_count;
    private String verified_description;
    private String st_color;
    private String intro;
    private boolean follow_me;
    private boolean allow_all_stock;
    private String verified_type;
    private String domain;
    private String type;
    private String location;
    private String description;
    private String id;
    private String url;
    private int status;
    private String screen_name;
    private boolean following;
    private boolean blocking;
    private String profile;
    private String verified;
    private String friends_count;
    private String followers_count;
    private int donate_count;
    private String name;
    private String verified_infos;
    private Live_info live_info;
    private String screenname_pinyin;
    private String group_ids;
    private boolean verified_realname;
    private String name_pinyin;
    private String photo_domain;
    private String profile_image_url;
    public void setSubscribeable(boolean subscribeable) {
         this.subscribeable = subscribeable;
     }
     public boolean getSubscribeable() {
         return subscribeable;
     }

    public void setCommon_count(int common_count) {
         this.common_count = common_count;
     }
     public int getCommon_count() {
         return common_count;
     }

    public void setRemark(String remark) {
         this.remark = remark;
     }
     public String getRemark() {
         return remark;
     }

    public void setRecommend_reason(String recommend_reason) {
         this.recommend_reason = recommend_reason;
     }
     public String getRecommend_reason() {
         return recommend_reason;
     }

    public void setStock_status_count(String stock_status_count) {
         this.stock_status_count = stock_status_count;
     }
     public String getStock_status_count() {
         return stock_status_count;
     }

    public void setProvince(String province) {
         this.province = province;
     }
     public String getProvince() {
         return province;
     }

    public void setCity(String city) {
         this.city = city;
     }
     public String getCity() {
         return city;
     }

    public void setGender(String gender) {
         this.gender = gender;
     }
     public String getGender() {
         return gender;
     }

    public void setBlog_description(String blog_description) {
         this.blog_description = blog_description;
     }
     public String getBlog_description() {
         return blog_description;
     }

    public void setStocks_count(String stocks_count) {
         this.stocks_count = stocks_count;
     }
     public String getStocks_count() {
         return stocks_count;
     }

    public void setStep(String step) {
         this.step = step;
     }
     public String getStep() {
         return step;
     }

    public void setRecommend(String recommend) {
         this.recommend = recommend;
     }
     public String getRecommend() {
         return recommend;
     }

    public void setLast_status_id(int last_status_id) {
         this.last_status_id = last_status_id;
     }
     public int getLast_status_id() {
         return last_status_id;
     }

    public void setStatus_count(String status_count) {
         this.status_count = status_count;
     }
     public String getStatus_count() {
         return status_count;
     }

    public void setVerified_description(String verified_description) {
         this.verified_description = verified_description;
     }
     public String getVerified_description() {
         return verified_description;
     }

    public void setSt_color(String st_color) {
         this.st_color = st_color;
     }
     public String getSt_color() {
         return st_color;
     }

    public void setIntro(String intro) {
         this.intro = intro;
     }
     public String getIntro() {
         return intro;
     }

    public void setFollow_me(boolean follow_me) {
         this.follow_me = follow_me;
     }
     public boolean getFollow_me() {
         return follow_me;
     }

    public void setAllow_all_stock(boolean allow_all_stock) {
         this.allow_all_stock = allow_all_stock;
     }
     public boolean getAllow_all_stock() {
         return allow_all_stock;
     }

    public void setVerified_type(String verified_type) {
         this.verified_type = verified_type;
     }
     public String getVerified_type() {
         return verified_type;
     }

    public void setDomain(String domain) {
         this.domain = domain;
     }
     public String getDomain() {
         return domain;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setLocation(String location) {
         this.location = location;
     }
     public String getLocation() {
         return location;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setUrl(String url) {
         this.url = url;
     }
     public String getUrl() {
         return url;
     }

    public void setStatus(int status) {
         this.status = status;
     }
     public int getStatus() {
         return status;
     }

    public void setScreen_name(String screen_name) {
         this.screen_name = screen_name;
     }
     public String getScreen_name() {
         return screen_name;
     }

    public void setFollowing(boolean following) {
         this.following = following;
     }
     public boolean getFollowing() {
         return following;
     }

    public void setBlocking(boolean blocking) {
         this.blocking = blocking;
     }
     public boolean getBlocking() {
         return blocking;
     }

    public void setProfile(String profile) {
         this.profile = profile;
     }
     public String getProfile() {
         return profile;
     }

    public void setVerified(String verified) {
         this.verified = verified;
     }
     public String getVerified() {
         return verified;
     }

    public void setFriends_count(String friends_count) {
         this.friends_count = friends_count;
     }
     public String getFriends_count() {
         return friends_count;
     }

    public void setFollowers_count(String followers_count) {
         this.followers_count = followers_count;
     }
     public String getFollowers_count() {
         return followers_count;
     }

    public void setDonate_count(int donate_count) {
         this.donate_count = donate_count;
     }
     public int getDonate_count() {
         return donate_count;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setVerified_infos(String verified_infos) {
         this.verified_infos = verified_infos;
     }
     public String getVerified_infos() {
         return verified_infos;
     }

    public void setLive_info(Live_info live_info) {
         this.live_info = live_info;
     }
     public Live_info getLive_info() {
         return live_info;
     }

    public void setScreenname_pinyin(String screenname_pinyin) {
         this.screenname_pinyin = screenname_pinyin;
     }
     public String getScreenname_pinyin() {
         return screenname_pinyin;
     }

    public void setGroup_ids(String group_ids) {
         this.group_ids = group_ids;
     }
     public String getGroup_ids() {
         return group_ids;
     }

    public void setVerified_realname(boolean verified_realname) {
         this.verified_realname = verified_realname;
     }
     public boolean getVerified_realname() {
         return verified_realname;
     }

    public void setName_pinyin(String name_pinyin) {
         this.name_pinyin = name_pinyin;
     }
     public String getName_pinyin() {
         return name_pinyin;
     }

    public void setPhoto_domain(String photo_domain) {
         this.photo_domain = photo_domain;
     }
     public String getPhoto_domain() {
         return photo_domain;
     }

    public void setProfile_image_url(String profile_image_url) {
         this.profile_image_url = profile_image_url;
     }
     public String getProfile_image_url() {
         return profile_image_url;
     }

}