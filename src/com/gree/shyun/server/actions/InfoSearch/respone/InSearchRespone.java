/**
  * Copyright 2022 json.cn 
  */
package com.gree.shyun.server.actions.InfoSearch.respone;
import java.util.List;

/**
 * Auto-generated: 2022-03-14 22:49:43
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class InSearchRespone {

    private int count;
    private int page;
    private List<MessageInfo> list;
    private int maxPage;
    public void setCount(int count) {
         this.count = count;
     }
     public int getCount() {
         return count;
     }

    public void setPage(int page) {
         this.page = page;
     }
     public int getPage() {
         return page;
     }

    public void setList(List<MessageInfo> list) {
         this.list = list;
     }
     public List<MessageInfo> getList() {
         return list;
     }

    public void setMaxPage(int maxPage) {
         this.maxPage = maxPage;
     }
     public int getMaxPage() {
         return maxPage;
     }

}