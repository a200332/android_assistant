package com.gree.shyun.server.actions.ChartMinute.respone;


/**
* Created by youdelu on 2022-03-08 13:53:55.
*
*/
public class Items{
	private Integer amount;//19831097
	private Capital capital;
	private Float chg;//-0.07
	private String macd;//null
	private Float avg_price;//3.591
	private Float percent;//-1.92
	private Integer volume;//5523900
	private String kdj;//null
	private Float current;//3.58
	private Float high;//3.7
	private Float low;//3.47
	private Volume_compare volume_compare;
	private Float timestamp;//1646616600000
	private String ratio;//null

	public void setAmount(Integer amount){
		this.amount=amount;
	}
	public Integer getAmount(){
		return amount;
	}

	public void setCapital(Capital capital){
		this.capital=capital;
	}
	public Capital getCapital(){
		return capital;
	}

	public void setChg(Float chg){
		this.chg=chg;
	}
	public Float getChg(){
		return chg;
	}

	public void setMacd(String macd){
		this.macd=macd;
	}
	public String getMacd(){
		return macd;
	}

	public void setAvg_price(Float avg_price){
		this.avg_price=avg_price;
	}
	public Float getAvg_price(){
		return avg_price;
	}

	public void setPercent(Float percent){
		this.percent=percent;
	}
	public Float getPercent(){
		return percent;
	}

	public void setVolume(Integer volume){
		this.volume=volume;
	}
	public Integer getVolume(){
		return volume;
	}

	public void setKdj(String kdj){
		this.kdj=kdj;
	}
	public String getKdj(){
		return kdj;
	}

	public void setCurrent(Float current){
		this.current=current;
	}
	public Float getCurrent(){
		return current;
	}

	public void setHigh(Float high){
		this.high=high;
	}
	public Float getHigh(){
		return high;
	}

	public void setLow(Float low){
		this.low=low;
	}
	public Float getLow(){
		return low;
	}

	public void setVolume_compare(Volume_compare volume_compare){
		this.volume_compare=volume_compare;
	}
	public Volume_compare getVolume_compare(){
		return volume_compare;
	}

	public void setTimestamp(Float timestamp){
		this.timestamp=timestamp;
	}
	public Float getTimestamp(){
		return timestamp;
	}

	public void setRatio(String ratio){
		this.ratio=ratio;
	}
	public String getRatio(){
		return ratio;
	}
}