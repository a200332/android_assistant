package com.gree.shyun.server.actions.ChartMinute.respone;


/**
* Created by youdelu on 2022-03-08 13:53:55.
*
*/
public class ChMinuteRespone{
	private Data data;
	private String error_description;//
	private Integer error_code;//0

	public void setData(Data data){
		this.data=data;
	}
	public Data getData(){
		return data;
	}

	public void setError_description(String error_description){
		this.error_description=error_description;
	}
	public String getError_description(){
		return error_description;
	}

	public void setError_code(Integer error_code){
		this.error_code=error_code;
	}
	public Integer getError_code(){
		return error_code;
	}
}