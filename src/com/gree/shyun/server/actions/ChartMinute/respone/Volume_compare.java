package com.gree.shyun.server.actions.ChartMinute.respone;


/**
* Created by youdelu on 2022-03-08 13:53:55.
*
*/
public class Volume_compare{
	private Integer volume_sum;//5523900
	private Integer volume_sum_last;//14491915

	public void setVolume_sum(Integer volume_sum){
		this.volume_sum=volume_sum;
	}
	public Integer getVolume_sum(){
		return volume_sum;
	}

	public void setVolume_sum_last(Integer volume_sum_last){
		this.volume_sum_last=volume_sum_last;
	}
	public Integer getVolume_sum_last(){
		return volume_sum_last;
	}
}