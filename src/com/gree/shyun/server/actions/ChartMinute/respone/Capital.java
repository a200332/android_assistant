package com.gree.shyun.server.actions.ChartMinute.respone;


/**
* Created by youdelu on 2022-03-08 13:53:55.
*
*/
public class Capital{
	private Float small;//-0.6
	private Float large;//1.79
	private Float xlarge;//-5.19
	private Float medium;//4.0

	public void setSmall(Float small){
		this.small=small;
	}
	public Float getSmall(){
		return small;
	}

	public void setLarge(Float large){
		this.large=large;
	}
	public Float getLarge(){
		return large;
	}

	public void setXlarge(Float xlarge){
		this.xlarge=xlarge;
	}
	public Float getXlarge(){
		return xlarge;
	}

	public void setMedium(Float medium){
		this.medium=medium;
	}
	public Float getMedium(){
		return medium;
	}
}