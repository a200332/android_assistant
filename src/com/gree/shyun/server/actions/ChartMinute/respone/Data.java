package com.gree.shyun.server.actions.ChartMinute.respone;


import java.util.List;

/**
* Created by youdelu on 2022-03-08 13:53:55.
*
*/
public class Data{
	private Float last_close;//3.65
	private String[] after;
	private List<Items> items;
	private Integer items_size;//242

	public void setLast_close(Float last_close){
		this.last_close=last_close;
	}
	public Float getLast_close(){
		return last_close;
	}

	public void setAfter(String[] after){
		this.after=after;
	}
	public String[] getAfter(){
		return after;
	}

	public void setItems(List<Items> items){
		this.items=items;
	}
	public List<Items> getItems(){
		return items;
	}

	public void setItems_size(Integer items_size){
		this.items_size=items_size;
	}
	public Integer getItems_size(){
		return items_size;
	}
}