package com.gree.shyun.server.actions.ChartMinute;


import com.gree.shyun.server.BaseAction;

import com.gree.shyun.server.actions.ChartMinute.requestParam.ChMinuteParam;
import com.gree.shyun.server.actions.ChartMinute.respone.ChMinuteRespone;

/**
 * ${remark}
 * Created by youdelu on 2022-03-08 13:53:55.
 */
public class ChMinuteAction extends BaseAction {
    public ChMinuteAction() {
        super();
    }
    public <T>T get(ChMinuteParam param) {
        return (T)get(DOMAIN,getURL("v5/stock/chart/minute.json",param),ChMinuteRespone.class , null );
    }
}