package com.gree.shyun.server.actions.ChartMinute.task;

import com.gree.shyun.server.actions.ChartMinute.ChMinuteAction;
import com.gree.shyun.server.actions.ChartMinute.requestParam.ChMinuteParam;
import com.gree.shyun.server.actions.ChartMinute.respone.ChMinuteRespone;
import com.gree.shyun.server.task.Task;

/**
* ${remark} 任务
* Created by youdelu on 2022-03-08 13:53:55.
*/
public class ChMinuteTask extends Task {
    final static String TAG = ChMinuteTask.class.getSimpleName();
    @Override
    public Task doTask() {
		ChMinuteParam chMinuteParam = new ChMinuteParam();
		chMinuteParam.setSymbol("SH600077");//SH600077,
		chMinuteParam.setPeriod("1d");//1d,
        ChMinuteAction action= new ChMinuteAction();
        try {
                ChMinuteRespone respone = action.get(chMinuteParam);
                set("respone",respone);
        } catch (Exception e) {
            e.printStackTrace();
            setStatus(ERROR);
            setException(e.getMessage());
        }
        return this;
    }
}