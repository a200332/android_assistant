package com.gree.shyun.server.actions.ChartMinute.task;

import com.gree.shyun.server.actions.ChartMinute.respone.ChMinuteRespone;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.server.task.TaskManager;
import com.gree.shyun.util.json.JsonMananger;

/**
 * 使用方法（测试用）
 * Created by youdelu on 2020-12-14 08:57:28.
 */
public class UseChMinuteTask {
    public static final String TAG = UseChMinuteTask.class.getSimpleName();
    public static void runTask(){
        ChMinuteTask chMinuteTask = new ChMinuteTask();
        chMinuteTask.set("data",TAG);//传递任意参数
        TaskManager.getInstance().exec(chMinuteTask, task->{
            if(task.getStatus()== Task.SUCCESS){
                ChMinuteRespone respone = task.getParam("respone");
                System.out.println(JsonMananger.beanToJsonStr(respone));
            }else{
                System.out.println("出错了:"+task.getException());
            }
        });
    }
}
