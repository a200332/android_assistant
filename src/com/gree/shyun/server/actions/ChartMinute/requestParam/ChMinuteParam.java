package com.gree.shyun.server.actions.ChartMinute.requestParam;


/**
* Created by youdelu on 2022-03-08 13:53:55.
*
*/
public class ChMinuteParam{
	private String symbol;//SH600077
	private String period;//1d

	public void setSymbol(String symbol){
		this.symbol=symbol;
	}
	public String getSymbol(){
		return symbol;
	}

	public void setPeriod(String period){
		this.period=period;
	}
	public String getPeriod(){
		return period;
	}
}