package com.gree.shyun.server.actions.QuoteList;


import com.gree.shyun.server.BaseAction;

import com.gree.shyun.server.actions.QuoteList.requestParam.QuListParam;
import com.gree.shyun.server.actions.QuoteList.respone.QuListRespone;

/**
 * ${remark}
 * Created by youdelu on 2022-03-08 14:03:22.
 */
public class QuListAction extends BaseAction {
    public QuListAction() {
        super();
    }
    public <T>T get(QuListParam param) {
        return (T)get(DOMAIN,getURL("/service/v5/stock/screener/quote/list",param),QuListRespone.class , null );
    }
}