package com.gree.shyun.server.actions.QuoteList.requestParam;


/**
* Created by youdelu on 2022-03-08 14:03:22.
*
*/
public class QuListParam{
	private String market;//CN
	private String size;//30
	private String orderby;//percent
	private String order_by;//percent
	private String page;//2
	private String type;//sh_sz
	private String order;//desc
	private String _;//1646317432247

	public void setMarket(String market){
		this.market=market;
	}
	public String getMarket(){
		return market;
	}

	public void setSize(String size){
		this.size=size;
	}
	public String getSize(){
		return size;
	}

	public void setOrderby(String orderby){
		this.orderby=orderby;
	}
	public String getOrderby(){
		return orderby;
	}

	public void setOrder_by(String order_by){
		this.order_by=order_by;
	}
	public String getOrder_by(){
		return order_by;
	}

	public void setPage(String page){
		this.page=page;
	}
	public String getPage(){
		return page;
	}

	public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return type;
	}

	public void setOrder(String order){
		this.order=order;
	}
	public String getOrder(){
		return order;
	}

	public void set_(String _){
		this._=_;
	}
	public String get_(){
		return _;
	}
}