package com.gree.shyun.server.actions.QuoteList.respone;


import com.gree.shyun.entity.Base;
import com.gree.shyun.server.db.annotation.Column;
import com.gree.shyun.server.db.annotation.Id;
import com.gree.shyun.server.db.annotation.NoAutoIncrement;
import com.gree.shyun.server.db.annotation.Table;

/**
* Created by youdelu on 2022-03-08 14:03:22.
*
*/
@Table
public class GpData extends Base {
	@Id
	@NoAutoIncrement
	@Column(desc = "编码")
	private String symbol;//SZ000151
	@Column(desc = "")
	private Float net_profit_cagr;//137.61622307971203
	@Column(desc = "")
	private String north_net_inflow;//null
	@Column(desc = "")
	private Float ps;//6.0808
	@Column(desc = "")
	private Integer type;//11
	@Column(desc = "涨幅")
	private Float percent;//10.02
	@Column(desc = "")
	private String has_follow;//false
	@Column(desc = "")
	private Float tick_size;//0.01
	@Column(desc = "")
	private Float pb_ttm;//4.2089
	@Column(desc = "")
	private Long float_shares;//266543967
	@Column(desc = "价格")
	private Float current;//10.65
	@Column(desc = "")
	private Float amplitude;//10.23
	@Column(desc = "")
	private String pcf;//null
	@Column(desc = "")
	private Float current_year_percent;//25.15
	@Column(desc = "")
	private Float float_market_capital;//2838693249
	@Column(desc = "")
	private String north_net_inflow_time;//null
	@Column(desc = "")
	private Float market_capital;//3592998253
	@Column(desc = "")
	private Integer dividend_yield;//0
	@Column(desc = "")
	private Integer lot_size;//100
	@Column(desc = "")
	private Float roe_ttm;//-36.97403528978749
	@Column(desc = "")
	private Float total_percent;//46.86
	@Column(desc = "")
	private Integer percent5m;//0
	@Column(desc = "")
	private Float income_cagr;//-28.56861057861141
	@Column(desc = "")
	private Float amount;//463750781.09
	@Column(desc = "")
	private Float chg;//0.97
	@Column(desc = "")
	private Float issue_date_ts;//968169600000
	@Column(desc = "")
	private Float eps;//-0.99
	@Column(desc = "")
	private Integer main_net_inflows;//51915211
	@Column(desc = "")
	private Integer volume;//45580649
	@Column(desc = "")
	private Float volume_ratio;//2.13
	@Column(desc = "")
	private Float pb;//4.209
	@Column(desc = "")
	private Integer followers;//56917
	@Column(desc = "")
	private Float turnover_rate;//17.1
	@Column(desc = "")
	private Float first_percent;//130.46
	@Column(desc = "名称")
	private String name;//中成股份
	@Column(desc = "")
	private String pe_ttm;//null
	@Column(desc = "")
	private Long total_shares;//337370728
	@Column(desc = "")
	private Integer limitup_days;//2

	public void setSymbol(String symbol){
		this.symbol=symbol;
	}
	public String getSymbol(){
		return symbol;
	}

	public void setNet_profit_cagr(Float net_profit_cagr){
		this.net_profit_cagr=net_profit_cagr;
	}
	public Float getNet_profit_cagr(){
		return net_profit_cagr;
	}

	public void setNorth_net_inflow(String north_net_inflow){
		this.north_net_inflow=north_net_inflow;
	}
	public String getNorth_net_inflow(){
		return north_net_inflow;
	}

	public void setPs(Float ps){
		this.ps=ps;
	}
	public Float getPs(){
		return ps;
	}

	public void setType(Integer type){
		this.type=type;
	}
	public Integer getType(){
		return type;
	}

	public void setPercent(Float percent){
		this.percent=percent;
	}
	public Float getPercent(){
		return percent;
	}

	public void setHas_follow(String has_follow){
		this.has_follow=has_follow;
	}
	public String getHas_follow(){
		return has_follow;
	}

	public void setTick_size(Float tick_size){
		this.tick_size=tick_size;
	}
	public Float getTick_size(){
		return tick_size;
	}

	public void setPb_ttm(Float pb_ttm){
		this.pb_ttm=pb_ttm;
	}
	public Float getPb_ttm(){
		return pb_ttm;
	}

	public void setFloat_shares(Long float_shares){
		this.float_shares=float_shares;
	}
	public Long getFloat_shares(){
		return float_shares;
	}

	public void setCurrent(Float current){
		this.current=current;
	}
	public Float getCurrent(){
		return current;
	}

	public void setAmplitude(Float amplitude){
		this.amplitude=amplitude;
	}
	public Float getAmplitude(){
		return amplitude;
	}

	public void setPcf(String pcf){
		this.pcf=pcf;
	}
	public String getPcf(){
		return pcf;
	}

	public void setCurrent_year_percent(Float current_year_percent){
		this.current_year_percent=current_year_percent;
	}
	public Float getCurrent_year_percent(){
		return current_year_percent;
	}

	public void setFloat_market_capital(Float float_market_capital){
		this.float_market_capital=float_market_capital;
	}
	public Float getFloat_market_capital(){
		return float_market_capital;
	}

	public void setNorth_net_inflow_time(String north_net_inflow_time){
		this.north_net_inflow_time=north_net_inflow_time;
	}
	public String getNorth_net_inflow_time(){
		return north_net_inflow_time;
	}

	public void setMarket_capital(Float market_capital){
		this.market_capital=market_capital;
	}
	public Float getMarket_capital(){
		return market_capital;
	}

	public void setDividend_yield(Integer dividend_yield){
		this.dividend_yield=dividend_yield;
	}
	public Integer getDividend_yield(){
		return dividend_yield;
	}

	public void setLot_size(Integer lot_size){
		this.lot_size=lot_size;
	}
	public Integer getLot_size(){
		return lot_size;
	}

	public void setRoe_ttm(Float roe_ttm){
		this.roe_ttm=roe_ttm;
	}
	public Float getRoe_ttm(){
		return roe_ttm;
	}

	public void setTotal_percent(Float total_percent){
		this.total_percent=total_percent;
	}
	public Float getTotal_percent(){
		return total_percent;
	}

	public void setPercent5m(Integer percent5m){
		this.percent5m=percent5m;
	}
	public Integer getPercent5m(){
		return percent5m;
	}

	public void setIncome_cagr(Float income_cagr){
		this.income_cagr=income_cagr;
	}
	public Float getIncome_cagr(){
		return income_cagr;
	}

	public void setAmount(Float amount){
		this.amount=amount;
	}
	public Float getAmount(){
		return amount;
	}

	public void setChg(Float chg){
		this.chg=chg;
	}
	public Float getChg(){
		return chg;
	}

	public void setIssue_date_ts(Float issue_date_ts){
		this.issue_date_ts=issue_date_ts;
	}
	public Float getIssue_date_ts(){
		return issue_date_ts;
	}

	public void setEps(Float eps){
		this.eps=eps;
	}
	public Float getEps(){
		return eps;
	}

	public void setMain_net_inflows(Integer main_net_inflows){
		this.main_net_inflows=main_net_inflows;
	}
	public Integer getMain_net_inflows(){
		return main_net_inflows;
	}

	public void setVolume(Integer volume){
		this.volume=volume;
	}
	public Integer getVolume(){
		return volume;
	}

	public void setVolume_ratio(Float volume_ratio){
		this.volume_ratio=volume_ratio;
	}
	public Float getVolume_ratio(){
		return volume_ratio;
	}

	public void setPb(Float pb){
		this.pb=pb;
	}
	public Float getPb(){
		return pb;
	}

	public void setFollowers(Integer followers){
		this.followers=followers;
	}
	public Integer getFollowers(){
		return followers;
	}

	public void setTurnover_rate(Float turnover_rate){
		this.turnover_rate=turnover_rate;
	}
	public Float getTurnover_rate(){
		return turnover_rate;
	}

	public void setFirst_percent(Float first_percent){
		this.first_percent=first_percent;
	}
	public Float getFirst_percent(){
		return first_percent;
	}

	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}

	public void setPe_ttm(String pe_ttm){
		this.pe_ttm=pe_ttm;
	}
	public String getPe_ttm(){
		return pe_ttm;
	}

	public void setTotal_shares(Long total_shares){
		this.total_shares=total_shares;
	}
	public Long getTotal_shares(){
		return total_shares;
	}

	public void setLimitup_days(Integer limitup_days){
		this.limitup_days=limitup_days;
	}
	public Integer getLimitup_days(){
		return limitup_days;
	}
}