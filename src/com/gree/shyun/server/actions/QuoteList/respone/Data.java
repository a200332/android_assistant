package com.gree.shyun.server.actions.QuoteList.respone;

import java.util.List;

/**
* Created by youdelu on 2022-03-08 14:03:22.
*
*/
public class Data{
	private Integer count;//4738
	private List<GpData> list;

	public void setCount(Integer count){
		this.count=count;
	}
	public Integer getCount(){
		return count;
	}

	public List<GpData> getList() {
		return list;
	}

	public void setList(List<GpData> list) {
		this.list = list;
	}
}