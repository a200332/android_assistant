package com.gree.shyun.server.actions.QuoteList.task;

import com.gree.shyun.server.actions.InfoSearch.task.InSearchTask;
import com.gree.shyun.server.actions.QuoteList.respone.GpData;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.db.bean.ColumnDesc;
import com.gree.shyun.server.task.TaskManager;
import com.gree.shyun.util.DataToExcelUtil;
import com.gree.shyun.util.json.JsonMananger;

import java.util.List;

/**
 * 使用方法（测试用）
 * Created by youdelu on 2020-12-14 08:57:28.
 */
public class UseQuListTask {
    public static final String TAG = UseQuListTask.class.getSimpleName();
    public static void runTask(){
        QuListTask quListTask = new QuListTask();
        TaskManager.getInstance().exec(quListTask, task->{
            if(task.success()){

            }else{
                System.out.println("出错了:"+task.getException());
            }
        });
    }
    public static void runMessage(){
        InSearchTask inSearchTask = new InSearchTask();
        TaskManager.getInstance().exec(inSearchTask,t -> {
            if(t.success()){
                System.out.println("加载完成。");
            }
        });
    }
    public static void main(String[] args){
        DbHelper.getInstance();
        List<ColumnDesc> descList = DbHelper.getTableColumnDesc(GpData.class);
        List<GpData> gpData = DbHelper.findAll(GpData.class);
        DataToExcelUtil.getExcelByDb(GpData.class);
        System.out.println(JsonMananger.beanToJsonStr(descList));
        //UseQuListTask.runMessage();
    }
}
