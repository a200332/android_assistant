package com.gree.shyun.server.actions.QuoteList.task;

import com.gree.shyun.server.actions.QuoteList.QuListAction;
import com.gree.shyun.server.actions.QuoteList.requestParam.QuListParam;
import com.gree.shyun.server.actions.QuoteList.respone.Data;
import com.gree.shyun.server.actions.QuoteList.respone.QuListRespone;
import com.gree.shyun.server.db.DbHelper;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.util.CommonUtil;

import java.util.Date;

/**
* ${remark} 任务
* Created by youdelu on 2022-03-08 14:03:22.
*/
public class QuListTask extends Task {
    final static String TAG = QuListTask.class.getSimpleName();
    @Override
    public Task doTask() {
        //加载全部分页数据
        getNextWidthAll();
        return this;
    }

    /**
     * 加载全部分页数据
     */
    private int pageSize = 100;
    private int pageIndex = 1;
    private int allPage = 0;
    private void getNextWidthAll(){
        QuListParam quListParam = new QuListParam();
        quListParam.setMarket("CN");//CN,
        quListParam.setSize(pageSize+"");//30,
        quListParam.setOrderby("percent");//percent,
        quListParam.setOrder_by("percent");//percent,
        quListParam.setPage(pageIndex+"");//2,
        quListParam.setType("sh_sz");//sh_sz,
        quListParam.setOrder("desc");//desc,
        quListParam.set_(new Date().getTime()+"");//1646317432247,
        QuListAction action= new QuListAction();

        QuListRespone respone = action.get(quListParam);
        Data data = respone.getData();
        //保存全部
        DbHelper.saveOrUpdate(data.getList());
        allPage = CommonUtil.getAllPage(data.getCount(),pageSize);
        pageIndex++;
        if(pageIndex<allPage){
            getNextWidthAll();
        }else{
            pageIndex = 1;
        }
    }


}