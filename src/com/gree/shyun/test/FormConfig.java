package com.gree.shyun.test;


public class FormConfig {

    public static final Object[][] cpxxAz = new Object[][]{
            {"pl","品类","请选择品类", Form.TYPE_SELECT,true,true},
            {"xl","小类","请选择小类", Form.TYPE_SELECT,true,true},
            {"xilie","系列","请选择系列", Form.TYPE_SELECT,true,false},
            {"cpxh","产品型号","请输入产品型号", Form.TYPE_DEFAULT,true,true},
            {"shul","数量","请输入数量", Form.TYPE_DEFAULT,true,true,"套"},
            {"wldm","物料代码","请输入物料代码", Form.TYPE_DEFAULT,true,false},
            {"dj","单价","请输入单价", Form.TYPE_DEFAULT,true,false}
    };
    public static final Object[][] cpxxXxtth = new Object[][]{
            {"pl","品类","请选择品类", Form.TYPE_SELECT,true,true},
            {"gdlcxz","工单流程选择","先申请后退换货|先退换货后申请", Form.TYPE_RADIO,true,true},
            {"gdlxxz","工单类型选择","退货工单|换货工单", Form.TYPE_RADIO,true,true},
            {"jqtm","机器条码","请输入机器条码", Form.TYPE_DEFAULT,true,false},
            {"xl","小类","请选择小类", Form.TYPE_SELECT,true,false},
            {"xilie","系列","请选择系列", Form.TYPE_SELECT,true,false},
            {"gmdj","购买单价","请输入购买单价", Form.TYPE_DEFAULT,true,false},
            {"cpxh","产品型号","请输入产品型号", Form.TYPE_DEFAULT,true,false},
            {"shul","数量","请输入数量", Form.TYPE_DEFAULT,true,true,"套"}
    };

    public static final Object[][] gzjqxx = new Object[][]{
            {"xsdw","销售单位","请输入销售单位", Form.TYPE_DEFAULT,true,false},
            {"fph","发票号","请输入发票号", Form.TYPE_DEFAULT,true,false},
            {"ccrq","出厂日期","请选择出厂日期", Form.TYPE_SELECT_DATE,false,false},
            {"azdw","安装单位","请输入安装单位", Form.TYPE_DEFAULT,true,false},
            {"azrq","安装日期","请选择安装日期", Form.TYPE_SELECT_DATE,true,false},
            {"jddw","鉴定单位","请输入鉴定单位", Form.TYPE_DEFAULT,true,false},
            {"jdrq","鉴定日期","请选择鉴定日期", Form.TYPE_SELECT_DATE,true,false},
            {"njxh","内机型号","请输入内机型号", Form.TYPE_DEFAULT,true,false},
            {"njtm","内机条码","请输入内机条码", Form.TYPE_DEFAULT,true,false},
            {"wjxh","外机型号","请输入外机型号", Form.TYPE_DEFAULT,true,false},
            {"wjtm","外机条码","请输入外机条码", Form.TYPE_DEFAULT,true,false}
    };

    public static final Object[][] gznn = new Object[][]{
            {"pl","品类","请选择品类", Form.TYPE_SELECT,true,false},
            {"xiaolei","小类","请选择小类", Form.TYPE_SELECT,true,false},
            {"xilie","系列","请输入系列", Form.TYPE_DEFAULT,true,false},
            {"cpxh","产品型号","请输入产品型号", Form.TYPE_DEFAULT,true,false},
            {"gzxx","故障现象","请选择故障现象", Form.TYPE_SELECT,true,false},
            {"njtm","内机条码","请输入内机条码", Form.TYPE_DEFAULT,true,false},
            {"gmrq","购买日期","请选择购买日期", Form.TYPE_SELECT,true,false},
            {"yxj","优先级","请选择优先级", Form.TYPE_SELECT,true,false},
            {"xsdw","销售单位","请选择销售单位", Form.TYPE_SELECT,true,false},
            {"xsdwdh","销售单位电话","请输入销售单位电话", Form.TYPE_DEFAULT,true,false},
            {"fwdw","服务单位","请输入服务单位", Form.TYPE_DEFAULT,true,false},
            {"fwdwdh","服务单位电话","请输入服务单位电话", Form.TYPE_DEFAULT,true,false},
            {"bxcs","报修次数","请输入保修次数", Form.TYPE_DEFAULT,true,false},
            {"tmjscs","条码结算次数","请输入条码结算次数", Form.TYPE_DEFAULT,true,false},
            {"ybsc","延保时长","月份", Form.TYPE_DEFAULT,true,false,"月"},
            {"bxdq","包修到期","请选择包修到期日期", Form.TYPE_SELECT,true,false},
            {"xxxx","详细信息","(限300字)请当前网点在此添加进行备注，备注信息允许更改!", Form.TYPE_TEXT,true,false}
    };

    public static final Object[][] jbxxAz = new Object[][]{
            {"qh","区号","请输入区号", Form.TYPE_DEFAULT,true,false},
            {"gh","固话","请输入固话", Form.TYPE_DEFAULT,true,false},
            {"ssqy","所属区域","请输入所属区域", Form.TYPE_DEFAULT,false,false},
            {"yddh2","移动电话2","请输入移动电话2", Form.TYPE_DEFAULT,true,false},
            {"ly","来源","请选择来源", Form.TYPE_SELECT,true,false},
            {"vipdj","VIP等级","请选择VIP等级", Form.TYPE_SELECT,true,false},
            {"fphm","发票号码","请输入发票号码", Form.TYPE_DEFAULT,true,false},
            {"kqbm","跨区编码","请输入跨区编码", Form.TYPE_DEFAULT,true,false},
            {"qwsmsj","期望上门时间","请选择期望上门时间", Form.TYPE_SELECT,true,false}
    };
    public static final Object[][] jbxxXxtth = new Object[][]{
            {"qwsmsj","期望上门时间","请选择期望上门时间", Form.TYPE_SELECT,true,false},
            {"bz","备注(退货原因)","请输入备注", Form.TYPE_TEXT,true,false},
            {"qh","区号","请输入区号", Form.TYPE_DEFAULT,true,false},
            {"gh","固话","请输入固话", Form.TYPE_DEFAULT,true,false},
            {"xsdh","销售单号","请输入销售单号", Form.TYPE_DEFAULT,true,true},
            {"xslx","销售类型","请选择销售类型", Form.TYPE_SELECT,true,false},
            {"ssqy","所属区域","请输入所属区域", Form.TYPE_DEFAULT,true,false},
            {"gcbh","工程编号","请输入工程编号", Form.TYPE_DEFAULT,false,false},
            {"gcmc","工程名称","请输入工程名称", Form.TYPE_DEFAULT,false,false},
            {"xsdw","销售单位","请选择销售单位", Form.TYPE_SELECT,true,false},
            {"gmrq","购买日期","请选择购买日期", Form.TYPE_SELECT,true,true},
            {"azrq","安装日期","请选择安装日期", Form.TYPE_SELECT,true,false}
    };
    public static final Object[][] jbxxWx = new Object[][]{
            {"qh","区号","请输入区号", Form.TYPE_DEFAULT,true,false},
            {"gh","固话","请输入固话", Form.TYPE_DEFAULT,true,false},
            {"ldhm","来电号码","请输入来电号码", Form.TYPE_DEFAULT,false,false},
            {"sj2","手机2","请输入手机2", Form.TYPE_DEFAULT,true,false},
            {"wxqy","维修区域","请输入维修区域", Form.TYPE_DEFAULT,false,false},
            {"vipdj","VIP等级","请选择VIP等级", Form.TYPE_SELECT,true,false}
    };

    public static final Object[][] jdxx = new Object[][]{
            {"sqr","申请人","请输入申请人", Form.TYPE_DEFAULT,false,false},
            {"sqdw","申请单位","请输入申请单位", Form.TYPE_DEFAULT,false,false},
            {"thhlx","退换货类型","请选择退换货类型", Form.TYPE_SELECT,true,false},
            {"xjxh","新机型号","请输入新机型号", Form.TYPE_DEFAULT,true,false},
            {"sctphwj","上传图片和文件","", Form.TYPE_TITLE,false,false},
            {"tmtp","条码图片","图片最多上传5张且单张不超过3M", Form.TYPE_IMAGE,true,false,5},
            {"gzdmtp","故障代码图片","图片最多上传5张且单张不超过3M", Form.TYPE_IMAGE,true,false,5},
            {"lsbw","漏水部位","图片最多上传5张且单张不超过3M", Form.TYPE_IMAGE,true,false,5},
            {"zysp","噪音视频","视频最多上传一个且大小不超过20M", Form.TYPE_VIDEO,true,false,1},
            {"zysp","上墙及排水环境照片（异味提供）","图片最多上传5张且单张不超过3M", Form.TYPE_IMAGE,true,false,5},
            {"jcytp","检测仪图片（系统问题提供）","图片最多上传5张且单张不超过3M", Form.TYPE_IMAGE,true,false,5},
            {"jdnrt","鉴定内容","", Form.TYPE_TITLE,false,false},
            {"jdnr","","例:用户**年**月保修空调**故障,经我单位上门检验发现***********导致空调故障", Form.TYPE_TEXT,true,false},
    };

    public static final Object[][] qtxx = new Object[][]{
            {"zj","支架","支架", Form.TYPE_DEFAULT,true,false},
            {"jcg","加长管","加长管", Form.TYPE_DEFAULT,true,false},
            {"kqkg","空气开关","空气开关", Form.TYPE_DEFAULT,true,false},
            {"gkzy","高空作业","高空作业", Form.TYPE_DEFAULT,true,false},
            {"kq","墙孔","墙孔", Form.TYPE_DEFAULT,true,false},
            {"qtfy","其他费用","其他费用", Form.TYPE_DEFAULT,true,false},
            {"fyhj","费用合计","费用合计", Form.TYPE_DEFAULT,true,false},
            {"bzxx","备注信息","(限300字)", Form.TYPE_TEXT,true,false}
    };

    public static final Object[][] wxnn = new Object[][]{
            {"xqlb","需求类别","请选择需求类别", Form.TYPE_SELECT,true,true},
            {"xqxl","需求小类","请选择需求小类", Form.TYPE_SELECT,true,false},
            {"qudao","渠道","请选择渠道", Form.TYPE_SELECT,true,true},
            {"qwsmsj","期望上门时间","请选择期望上门时间", Form.TYPE_SELECT,true,false},
            {"beiz","备注","(限500字)请在此创建备注,备注录入后下级不允许更改!", Form.TYPE_TEXT,true,false}
    };

    public static final Object[][] xsxx = new Object[][]{
            {"xsdh","销售单号","请输入销售单号", Form.TYPE_DEFAULT,true,true},
            {"xslx","销售类型","请选择销售类型", Form.TYPE_SELECT,true,true},
            {"gmsj","购买时间","请选择购买时间", Form.TYPE_SELECT,true,true},
            {"xsdw","销售单位","请选择销售单位", Form.TYPE_SELECT,true,true}
    };

    public static final Object[][] yhtsnr = new Object[][]{
            {"bt","开机20分钟制冷(热)检测","", Form.TYPE_TITLE,true,false},
            {"swwd","室外温度","请输入室外温度", Form.TYPE_DEFAULT,true,false},
            {"snwd","室内温度","请输入室内温度", Form.TYPE_DEFAULT,true,false},
            {"qddy","启动电压","请输入启动电压", Form.TYPE_DEFAULT,true,false},
            {"qddl","启动电流","请输入启动电流", Form.TYPE_DEFAULT,true,false},
            {"yddy","运行电压","请输入运行电压", Form.TYPE_DEFAULT,true,false},
            {"yddl","运行电流","请输入运行电流", Form.TYPE_DEFAULT,true,false},
            {"gyyl","高压压力","请输入高压压力", Form.TYPE_DEFAULT,true,false},
            {"dyyl","低压压力","请输入低压压力", Form.TYPE_DEFAULT,true,false},
            {"cfkwd","出风口温度","请输入出风口温度", Form.TYPE_DEFAULT,true,false},
            {"wc","温差","请输入温差", Form.TYPE_DEFAULT,true,false},
            {"bj","开机50分钟制冷(热)检测","", Form.TYPE_TITLE,true,false},
            {"swwd5","室外温度","请输入室外温度", Form.TYPE_DEFAULT,true,false},
            {"snwd5","室内温度","请输入室内温度", Form.TYPE_DEFAULT,true,false},
            {"yxdy5","运行电压","请输入运行电压", Form.TYPE_DEFAULT,true,false},
            {"yxdl5","运行电流","请输入运行电流", Form.TYPE_DEFAULT,true,false},
            {"gyyl5","高压压力","请输入高压压力", Form.TYPE_DEFAULT,true,false},
            {"dyyl5","低压压力","请输入低压压力", Form.TYPE_DEFAULT,true,false},
            {"cfkwd5","出风口温度","请输入出风口温度", Form.TYPE_DEFAULT,true,false},
            {"hfkwd5","回风口温度","请输入回风口温度", Form.TYPE_DEFAULT,true,false},
            {"wc5","温差","请输入温差", Form.TYPE_DEFAULT,true,false},
            {"ppeddl","品牌额定电流","请输入品牌额定电流", Form.TYPE_DEFAULT,true,false}
    };




    /**
     * 维修工单模块
     */
    public static final Object[][] wxCreate = {
            {"基本信息",0,jbxxWx},
            {"维修内容",1,wxnn},
            {"故障内容",2,gznn},
            {"多品类维修",3}
    };

    /**
     * 安装工单模块 和 配送 共用
     */
    public static final Object[][] azCreate = {
            {"基本信息",0,jbxxAz},
            {"销售信息",1,xsxx},
            {"产品信息",2,cpxxAz},
            {"其他信息",3,qtxx}
    };

    /**
     * 线下退换货模块
     */
    public static final Object[][] xxtthCreate = {
            {"产品信息",0,cpxxXxtth},
            {"基本信息",1,jbxxXxtth}
    };

    /**
     * 总部退换货模块
     */
    public static final Object[][] zbtthCreate = {
            {"鉴定信息",0,jdxx},
            {"故障机器信息",1,gzjqxx},
            {"用户投诉内容",2,yhtsnr}
    };

}
