package com.gree.shyun.test;

import com.gree.shyun.util.FolderFileScanner;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAllUrl {

    public static String DOMAIN_LOGIN = "https://"+getIdmHost()+".gree.com";
    //手机端
    public static String DOMAIN_FLYDIY = "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshmobile/api";
    //ESB通道
    public static String DOMAIN_FLYDIY_ESB = "https://"+getApiHost()+".gree.com/api/esb/autoapp-default-server-greeshmobile/api";
    //文件上传
    public static String DOMAIN_FLYDIY_UPLOAD = "https://"+getApiHost()+".gree.com/api/sso/nts-foundation-attachmentmanager/api/v2";


    //安装
    public static String DOMAIN_INSTALL= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshinstall/api";
    //维修
    public static String DOMAIN_REPAIR= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshservice/api";
    //合并
    public static String DOMAIN_MERGE= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshmerge/api";

    //消息队列
    public static String DOMAIN_KAFKA= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-wxkafkaproducer/api";
    //自动派工
    public static String AUTO_RULE="https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshautorule/api";

    //配送
    public static String DOMAIN_DELIVERYJOBS ="https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greedeliveryjobs/api";

    //线下退换货
    public static String DOMAIN_RETURNGOODS = "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshreturngoods/api";
    //主数据
    public static String DOMAIN_MASTERDATA= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshmasterdata/api";

    //权限
    public static String DOMAIN_PERMISSION= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-permission/api";
    public static String DOMAIN_OFFLINE_RETURN_OR_EXCHANGE= "https://"+getApiHost()+".gree.com/api/sso/autoapp-default-server-greeshreturngoods";

    public static String getApiHost(){
        if(true){
            return "api-sms";
        }else{
            return "apishyun";
        }
    }
    public static String getIdmHost(){
        if(true){
            return "idm-sms";
        }else{
            return "idmshyun";
        }
    }
    public static void main(String[] args){
        String path = "F:/greeshyun/app/src/main/java/com/gree/shyun/";


        Map<String,String> map = new HashMap<>();
        map.put("DOMAIN_LOGIN",DOMAIN_LOGIN);
        map.put("DOMAIN_FLYDIY",DOMAIN_FLYDIY);
        map.put("DOMAIN_FLYDIY_ESB",DOMAIN_FLYDIY_ESB);
        map.put("DOMAIN_FLYDIY_UPLOAD",DOMAIN_FLYDIY_UPLOAD);
        map.put("DOMAIN_INSTALL",DOMAIN_INSTALL);
        map.put("DOMAIN_REPAIR",DOMAIN_REPAIR);
        map.put("DOMAIN_MERGE",DOMAIN_MERGE);
        map.put("DOMAIN_KAFKA",DOMAIN_KAFKA);
        map.put("AUTO_RULE",AUTO_RULE);
        map.put("DOMAIN_DELIVERYJOBS",DOMAIN_DELIVERYJOBS);
        map.put("DOMAIN_RETURNGOODS",DOMAIN_RETURNGOODS);
        map.put("DOMAIN_MASTERDATA",DOMAIN_MASTERDATA);
        map.put("DOMAIN_PERMISSION",DOMAIN_PERMISSION);
        map.put("DOMAIN_OFFLINE_RETURN_OR_EXCHANGE",DOMAIN_OFFLINE_RETURN_OR_EXCHANGE);

        try {
            ArrayList<String> scanFiles  = FolderFileScanner.scanFilesWithRecursion(path);
            for(String filePath:scanFiles){
                File file = new File(filePath);
                List<String> lines = FileUtils.readLines(file);
                for(String line:lines){
                    line = line.trim();
                    if(line.startsWith("return (T)")){
                        try {
                            line = line.replaceAll(" ","");
                            String domain = line.substring(line.lastIndexOf("(")+1,line.indexOf(",\""));
                            String url = line.substring(line.indexOf(",\"")+2,line.indexOf("\","));
                            System.out.println(map.get(domain)+"/"+url);
                        }catch (Exception e){

                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
