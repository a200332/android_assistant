package com.gree.shyun.test;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println(firstUpper("cpxxAz")+toFormList(FormConfig.cpxxAz));
        System.out.println(firstUpper("cpxxXxtth")+toFormList(FormConfig.cpxxXxtth));
        System.out.println(firstUpper("gzjqxx")+toFormList(FormConfig.gzjqxx));
        System.out.println(firstUpper("gznn")+toFormList(FormConfig.gznn));
        System.out.println(firstUpper("jbxxAz")+toFormList(FormConfig.jbxxAz));
        System.out.println(firstUpper("jbxxXxtth")+toFormList(FormConfig.jbxxXxtth));
        System.out.println(firstUpper("jbxxWx")+toFormList(FormConfig.jbxxWx));
        System.out.println(firstUpper("jdxx")+toFormList(FormConfig.jdxx));
        System.out.println(firstUpper("qtxx")+toFormList(FormConfig.qtxx));
        System.out.println(firstUpper("wxnn")+toFormList(FormConfig.wxnn));
        System.out.println(firstUpper("xsxx")+toFormList(FormConfig.xsxx));
        System.out.println(firstUpper("yhtsnr")+toFormList(FormConfig.yhtsnr));
    }
    public static String firstUpper(String s){
        return s.substring(0,1).toUpperCase()+s.substring(1)+"\n";
    }
    public static String toFormList(Object[][] obj){
        String temp = "";
        int i = 0 ;
        for(Object[] f:obj){
            if(f.length<6){
                continue;
            }
            temp+="@FormColumn(title = \""+(String)f[1]+"\"";
            temp+=",hint=\""+(String)f[2]+"\"";
            temp+=",type="+getType((int)f[3]);
            temp+=",enabled="+String.valueOf((boolean)f[4]);
            temp+=",required="+String.valueOf((boolean)f[5]);

            if(f.length>6){
                Object f6 = f[6];
                if(f6 instanceof  Integer){
                    temp+=",length="+(int)f6;
                }else {
                    temp+=",danw=\""+String.valueOf(f[6])+"\"";
                }
            }
            if(f.length>7){
                temp+=",length="+(int)f[7];
            }
            temp+=",order="+i;
            temp+=")\n";
            temp+="private String "+(String)f[0]+";\n";

            int type = (int)f[3];
            if(type==Form.TYPE_SELECT
                    ||type==Form.TYPE_SELECT_DATE){
                temp+="private String "+(String)f[0]+"Value;\n";
            }
            i++;
        }
        return temp;
    }
    private static String getType(int type){
        if(type==Form.TYPE_DEFAULT){
            return "Form.TYPE_DEFAULT";
        }else if(type==Form.TYPE_SELECT){
            return "Form.TYPE_SELECT";
        }else if(type==Form.TYPE_SELECT_DATE){
            return "Form.TYPE_SELECT_DATE";
        }else if(type==Form.TYPE_TEXT){
            return "Form.TYPE_TEXT";
        }else if(type==Form.TYPE_RADIO){
            return "Form.TYPE_RADIO";
        }else if(type==Form.TYPE_TITLE){
            return "Form.TYPE_TITLE";
        }else if(type==Form.TYPE_IMAGE){
            return "Form.TYPE_IMAGE";
        }else if(type==Form.TYPE_VIDEO){
            return "Form.TYPE_VIDEO";
        }
        return "Form.TYPE_DEFAULT";
    }
}
