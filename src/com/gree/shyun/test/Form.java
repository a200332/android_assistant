package com.gree.shyun.test;

/**
 * 表单
 */
public class Form {
    public static final int TYPE_DEFAULT = 0 ;//默认输入框
    public static final int TYPE_SELECT = 1;//默认选择框
    public static final int TYPE_SELECT_DATE = 2;//日期选择框
    public static final int TYPE_TEXT= 3;//多行文本输入框
    public static final int TYPE_RADIO = 4;//单选框
    public static final int TYPE_TITLE = 5;//标题栏
    public static final int TYPE_IMAGE = 6;//图片
    public static final int TYPE_VIDEO = 7;//视频
    public static final int TYPE_LINE= 8;//单行表格查看
    private String id;//id
    private String groupId;//分组ID
    private String title;//标题
    private String hint;//默认提示
    private String tips;//错误提示
    private int type;//类型
    private String value;//值
    private boolean enabled;//是否禁用
    private boolean required;//必填
    private String danw;//单位
    private int length;//最大长度

    private String[] regexVerify;//正则验证
    private String[] regexTips;//正则验证错误提示

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDanw() {
        return danw;
    }

    public void setDanw(String danw) {
        this.danw = danw;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String[] getRegexVerify() {
        return regexVerify;
    }

    public void setRegexVerify(String[] regexVerify) {
        this.regexVerify = regexVerify;
    }

    public String[] getRegexTips() {
        return regexTips;
    }

    public void setRegexTips(String[] regexTips) {
        this.regexTips = regexTips;
    }
}
