package com.gree.shyun.ui.orderdetail.com.gree.shyun.bean;

import com.gree.shyun.ui.orderdetail.annotation.FormColumn;
import com.gree.shyun.ui.orderdetail.com.gree.shyun.entity.Form;
import com.gree.shyun.com.gree.shyun.bean.BaseBean;
import com.gree.shyun.server.db.annotation.Table;

@Table
public class SearchKjmm extends BaseBean{
		@FormColumn(title="家/商用",type=Form.TYPE_SELECT,enabled=true,required=false,length=300,order=0)
		private String j/sy;
		@FormColumn(title="安装条码",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=1)
		private String aztm;
		@FormColumn(title="安装人",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=2)
		private String azr;
		@FormColumn(title="网点编号",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=3)
		private String wdbh;
		@FormColumn(title="开机时间",type=Form.TYPE_SELECT_DATE,enabled=true,required=false,length=300,order=4)
		private String kjsj;
		public String getJ/sy(){ return j/sy;}
		public void setJ/sy(String j/sy){ 
			this.j/sy = j/sy;
		}
		public String getAztm(){ return aztm;}
		public void setAztm(String aztm){ 
			this.aztm = aztm;
		}
		public String getAzr(){ return azr;}
		public void setAzr(String azr){ 
			this.azr = azr;
		}
		public String getWdbh(){ return wdbh;}
		public void setWdbh(String wdbh){ 
			this.wdbh = wdbh;
		}
		public String getKjsj(){ return kjsj;}
		public void setKjsj(String kjsj){ 
			this.kjsj = kjsj;
		}
}