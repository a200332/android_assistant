package com.gree.shyun.ui.orderdetail.com.gree.shyun.bean;

import com.gree.shyun.ui.orderdetail.annotation.FormColumn;
import com.gree.shyun.ui.orderdetail.com.gree.shyun.entity.Form;
import com.gree.shyun.com.gree.shyun.bean.BaseBean;
import com.gree.shyun.server.db.annotation.Table;

@Table
public class Scxx extends BaseBean{
		@FormColumn(title="订单行号",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=0)
		private String ddxh;
		@FormColumn(title="电商标识",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=1)
		private String dsbs;
		@FormColumn(title="店铺编号",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=2)
		private String dpbh;
		@FormColumn(title="店铺名称",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=3)
		private String dpmc;
		@FormColumn(title="下单电话",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=4)
		private String xddh;
		@FormColumn(title="新零售",type=Form.TYPE_DEFAULT,enabled=true,required=false,length=300,order=5)
		private String xls;
		public String getDdxh(){ return ddxh;}
		public void setDdxh(String ddxh){ 
			this.ddxh = ddxh;
		}
		public String getDsbs(){ return dsbs;}
		public void setDsbs(String dsbs){ 
			this.dsbs = dsbs;
		}
		public String getDpbh(){ return dpbh;}
		public void setDpbh(String dpbh){ 
			this.dpbh = dpbh;
		}
		public String getDpmc(){ return dpmc;}
		public void setDpmc(String dpmc){ 
			this.dpmc = dpmc;
		}
		public String getXddh(){ return xddh;}
		public void setXddh(String xddh){ 
			this.xddh = xddh;
		}
		public String getXls(){ return xls;}
		public void setXls(String xls){ 
			this.xls = xls;
		}
}