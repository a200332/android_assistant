(function(window){
    "use strict";
    var u = {};
	//拖动的view ,需要放置的view
	u.bind = function(id,putId,fn){
		//获取元素
		var dv = $api.byId(id);
		//放置的元素
		var pt = $api.byId(putId);
		var x = 0;
		var y = 0;
		var l = 0;
		var t = 0;
		var isDown = false; 
		//鼠标按下事件
		dv.onmousedown = function(e) {
		    //获取x坐标和y坐标
		    x = e.clientX;
		    y = e.clientY;
		
		    //获取左部和顶部的偏移量
		    l = dv.offsetLeft;
		    t = dv.offsetTop;
		    //开关打开
		    isDown = true;  
			var dvClassName = dv.firstChild.className;
			fn&&fn("onmousedown",dvClassName,e);
		}
		//鼠标移动
		window.onmousemove = function(e) {
		    if (isDown == false) {
		        return;
		    }
		    //获取x和y
		    var nx = e.clientX;
		    var ny = e.clientY;
		    //计算移动后的左偏移量和顶部的偏移量
		    var nl = nx - (x - l);
		    var nt = ny - (y - t);
		
		    dv.style.left = nl + 'px';
		    dv.style.top = nt + 'px';
			
			var dvWidth = dv.clientWidth;
			var dvHeight = dv.clientHeight;
			var ptLeft = u.getRect(pt).left;
			var ptTop = u.getRect(pt).top;
			var width = pt.offsetWidth;
			var height = pt.offsetHeight; 
			//进入放置区域
			if((nl+dvWidth)>=ptLeft
				&&nl<(ptLeft+width)
				&&(nt+dvHeight)>=ptTop
				&&nt<(ptTop+height)){
				 //在顶部区域 向上滚动
				 if(nt<(ptTop+20)){
					 if(pt.scrollTop>20){
						pt.scrollTop=pt.scrollTop-20; 
					 }
					 //在底部区域 向下滚动
				 }else if((nt+dvHeight)>=(ptTop+height-20)){
					 if(pt.scrollHeight>pt.scrollTop+20){
						pt.scrollTop=pt.scrollTop+20; 
					 }
				 }else{
					 if(pt.children.length>0){
						 //遍历所有子节点的位置信息
						 for(var i = 0;i<pt.children.length;i++){
						 	var child = pt.children[i];
							//只拿page-item
							if(child.className!=null){
								var cWidth = child.clientWidth;
								var cHeight = child.clientHeight;
								var cLeft = u.getRect(child).left;
								var cTop = u.getRect(child).top; 
								//上方插入备选区
								var html = "<div id='page-item-putplace' attr-id='"+i+"'>松开鼠标左键放置到此处</div>";
								if(nt>=cTop&&nt<cTop+(cHeight/2)){ 
									var putplace = $api.byId("page-item-putplace");
									if($api.attr(putplace,"attr-id")!=i){
										$api.remove(putplace);
										$(child).before(html); 
									}
									break;	
								//下方插入备选区
								}else if(nt>=cTop+(cHeight/2)&&nt<cTop+cHeight){ 
									var putplace = $api.byId("page-item-putplace");
									if($api.attr(putplace,"attr-id")!=i){
										$api.remove(putplace);
										$(child).after(html);  
									}
									break;
								}
							}
						 }
					 }else{ 
						 var html = "<div id='page-item-putplace' attr-id='"+i+"'>松开鼠标左键放置到此处</div>";
						 $(pt).html(html);  
					 }
					 
				 }
			} 
			
			var dvClassName = dv.firstChild.className;
			fn&&fn("onmousemove",dvClassName,e);
		}
		//鼠标抬起事件
		dv.onmouseup = function() {
		    //开关关闭
		    isDown = false; 
			dv.style.display = "none";
			var dvClassName = dv.firstChild.className;
			fn&&fn("onmouseup",dvClassName);
		}
	}
	u.getRect = function(obj){
		var sTop=document.documentElement.scrollTop == 0 ? document.body.scrollTop : document.documentElement.scrollTop;
		var sLeft= document.documentElement.scrollLeft == 0 ? document.body.scrollLeft : document.documentElement.scrollLeft;
		var left= obj.getBoundingClientRect().left + sLeft ;
		var top = obj.getBoundingClientRect().top + sTop ;
		return {'left':left,'top':top};
	}
	window.$drag = u;
})(window);