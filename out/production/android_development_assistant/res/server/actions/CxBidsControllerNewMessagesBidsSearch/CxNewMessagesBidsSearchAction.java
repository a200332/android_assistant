package com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch;

import android.content.Context;

import com.gree.shyun.server.BaseAction;

import com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.respone.CxNewMessagesBidsSearchRespone;
import com.gree.shyun.server.network.http.HttpException;


/**
 * ${remark}
 * Created by youdelu on 2022-04-29 11:12:49.
 */
public class CxNewMessagesBidsSearchAction extends BaseAction {
    public CxNewMessagesBidsSearchAction(Context context) {
        super(context);
    }
    public <T>T post() throws HttpException {
        return (T)postInFly(ROOT_DEFAULT,"CxBidsController/newMessagesBidsSearch",CxNewMessagesBidsSearchRespone.class , null );
    }
}