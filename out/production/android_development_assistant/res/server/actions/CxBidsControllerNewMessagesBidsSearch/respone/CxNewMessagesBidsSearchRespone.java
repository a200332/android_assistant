package com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.respone;

import java.com.gree.shyun.util.GpData;

/**
* Created by youdelu on 2022-04-29 11:12:50.
*
*/
public class CxNewMessagesBidsSearchRespone{
	private String exception;//null
	private Integer total;//1
	private GpData<Data> data;
	private String messageCode;//null
	private String message;//null
	private Integer statusCode;//200
	private String timestamp;//2022-04-29T03:09:48.068Z

	public void setException(String exception){
		this.exception=exception;
	}
	public String getException(){
		return exception;
	}

	public void setTotal(Integer total){
		this.total=total;
	}
	public Integer getTotal(){
		return total;
	}

	public void setData(GpData<Data> data){
		this.data=data;
	}
	public GpData<Data> getData(){
		return data;
	}

	public void setMessageCode(String messageCode){
		this.messageCode=messageCode;
	}
	public String getMessageCode(){
		return messageCode;
	}

	public void setMessage(String message){
		this.message=message;
	}
	public String getMessage(){
		return message;
	}

	public void setStatusCode(Integer statusCode){
		this.statusCode=statusCode;
	}
	public Integer getStatusCode(){
		return statusCode;
	}

	public void setTimestamp(String timestamp){
		this.timestamp=timestamp;
	}
	public String getTimestamp(){
		return timestamp;
	}
}