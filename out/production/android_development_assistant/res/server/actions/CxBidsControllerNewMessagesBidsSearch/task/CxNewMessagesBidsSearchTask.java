package com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.task;

import com.gree.shyun.App;
import com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.CxNewMessagesBidsSearchAction;
import com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.respone.CxNewMessagesBidsSearchRespone;
import com.gree.shyun.server.network.async.AsyncTaskManager;
import com.gree.shyun.server.network.http.HttpException;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.server.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
* ${remark} 任务
* Created by youdelu on 2022-04-29 11:12:49.
*/
public class CxNewMessagesBidsSearchTask extends Task {
    final static String TAG = CxNewMessagesBidsSearchTask.class.getSimpleName();
    @Override
    public Task doTask() {
        //测试代码
        String data = getParam("data");
        if(data!=null){
            LogUtil.d(TAG,"接收到了参数:"+data);
        }
        CxNewMessagesBidsSearchAction action= new CxNewMessagesBidsSearchAction(App.getApp());
        try {
                CxNewMessagesBidsSearchRespone respone = action.post();
                if(respone.getStatusCode()==200){
                    //处理返回数据
                    set("respone",respone);
                }else{
                    setStatus(ERROR);
                    set("code",respone.getStatusCode());
                    setException(respone.getMessage());
                }
        } catch (HttpException e) {
            e.printStackTrace();
            setStatus(ERROR);
            int state = AsyncTaskManager.getErrorState(e);
            set("code", state);
            setException(AsyncTaskManager.getErrMsgByState(state));
        }
        return this;
    }
}