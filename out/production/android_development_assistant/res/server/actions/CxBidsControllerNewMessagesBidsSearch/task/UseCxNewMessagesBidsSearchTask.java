package com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.task;

import com.gree.shyun.server.actions.CxBidsControllerNewMessagesBidsSearch.respone.CxNewMessagesBidsSearchRespone;
import com.gree.shyun.server.task.Task;
import com.gree.shyun.server.task.TaskManager;
import com.gree.shyun.server.utils.LogUtil;
import com.gree.shyun.server.utils.json.JsonMananger;

/**
 * 使用方法（测试用）
 * Created by youdelu on 2020-12-14 08:57:28.
 */
public class UseCxNewMessagesBidsSearchTask {
    public static final String TAG = UseCxNewMessagesBidsSearchTask.class.getSimpleName();
    public static void runTask(){
        CxNewMessagesBidsSearchTask cxNewMessagesBidsSearchTask = new CxNewMessagesBidsSearchTask();
        cxNewMessagesBidsSearchTask.set("data",TAG);//传递任意参数
        TaskManager.getInstance().exec(cxNewMessagesBidsSearchTask,task->{
            if(task.getStatus()== Task.SUCCESS){
                CxNewMessagesBidsSearchRespone respone = task.getParam("respone");
                LogUtil.d(TAG, "返回值:"+JsonMananger.beanToJsonStr(respone));
            }else{
                LogUtil.d(TAG, "出错了:"+task.getException());
            }
        });
    }
}
