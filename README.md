#  安卓开发助手

####  介绍
安卓开发助手

##### 本工具使用java开发，可运行于Windows/Mac/Linux终端，主要用于辅助android项目开发，自动生成java、xml代码，提高效率
##### 主要功能：
##### 1、自动生成android layout 界面代码(待开发)
##### 2、自动生成绑定view事件的代码(待开发)
##### 3、自动生成android表单的配置文件(普通文本，输入框，选择框，多图，日期..等)
##### 4、自动解析和管理由swigger 接口或Fiddler抓包获得的api，并一键生成所有相关的java com.gree.shyun.bean
##### 5、自动抓取APP调试长日志，生成表单
##### 6、一键生成各种规格的图片,生成二维码等，更多功能待开发
### 运行效果图
![avatar](img/1.bmp)
![avatar](img/2bmp)
![avatar](img/3.bmp)
![avatar](img/4.bmp)
![avatar](img/5.bmp)
![avatar](img/6.bmp)
![avatar](img/7.bmp)
###自动生成的代码效果
![avatar](img/8.bmp)
![avatar](img/9.bmp)
